
import 'package:cherchernom/presentation/core/widgets/n_button.dart';
import 'package:cherchernom/presentation/navigation/routes.dart';
import 'package:cherchernom/presentation/scaffold/loading_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../scaffold/scaffold_view.dart';
import 'contact_view_controller.dart';

class ContactView extends GetView<ContactViewController> {
  ContactView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onEmpty:    Text("Empty", style: TextStyle(color: Colors.black),),
      onLoading: LoadingScaffold(),
      onError: (e) {
        print(e);
        return LinearProgressIndicator();
      },
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      isHome: false,
      body: Center(
        child: Column(
          children: [
            Text("OHHH"),
           NButton(backgroundColor: Color(0xFF86229f),text: 'Login Page',onPressed: (){
             Get.toNamed(Routes.LOGIN);
           },)
          ],
        ),
      ),
    );
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
