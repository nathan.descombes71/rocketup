
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'contact_view_controller.dart';

class ContactViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => ContactViewController(
      ),
    );
  }
}
