import 'package:cherchernom/domain/feature/training_worskshop/controller/training_workshop_controller.dart';
import 'package:cherchernom/domain/feature/training_worskshop/entities/training_workshop.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/styles/styles.dart';

class TrainingWorkshopViewController extends GetxController
    with StateMixin, SingleGetTickerProviderMixin {
  final RxInt currentIndex = 0.obs;

  Rx<TextStyle> itemAppbarStyle =
      TextStyle(color: kTitleColorTransparent, fontSize: 20).obs;

  List<TrainingWorkshop> listWorkshop = [];

  RxList<TrainingWorkshop> filteredList = <TrainingWorkshop>[].obs;

  String nameFilter = '';

  RxDouble colorOpacityTraining = 0.45.obs;

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());
    listWorkshop = TrainingWorkshopController().listTrainingWorkshop;
    filteredList.addAll(listWorkshop);
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void filterList() {
    filteredList.clear();
    listWorkshop.forEach((element) {
      if (element.name.toLowerCase().contains(nameFilter.toLowerCase())) {
        filteredList.add(element);
      }
    });
  }
}
