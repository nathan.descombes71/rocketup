import 'package:cherchernom/domain/feature/training_worskshop/entities/training_workshop.dart';
import 'package:cherchernom/presentation/core/clip/nav_bar_clipper.dart';
import 'package:cherchernom/presentation/core/clip/training_code_clipper.dart';
import 'package:cherchernom/presentation/core/styles/styles.dart';
import 'package:cherchernom/presentation/core/widgets/n_button.dart';
import 'package:cherchernom/presentation/navigation/routes.dart';
import 'package:cherchernom/presentation/scaffold/loading_scaffold.dart';
import 'package:cherchernom/presentation/views/web/training%20_workshop/training_workshop_view_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../scaffold/scaffold_view.dart';

class TrainingWorkshopView extends GetView<TrainingWorkshopViewController> {
  TrainingWorkshopView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onEmpty: Text(
        "Empty",
        style: TextStyle(color: Colors.black),
      ),
      onLoading: LoadingScaffold(),
      onError: (e) {
        print(e);
        return LinearProgressIndicator();
      },
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      isHome: false,
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(100, 25, 50, 25),
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 50, bottom: 25),
                  height: 50,
                  width: 400,
                  child: TextField(
                    onChanged: (value) {
                      controller.nameFilter = value;
                      controller.filterList();
                    },
                    decoration: InputDecoration(
                        hintText: "Entrez un nom",
                        hintStyle: kTextStyle.copyWith(
                            color: Colors.black, fontSize: 20)),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 50, right: 50),
            child: Obx(
              () => Wrap(
                alignment: controller.filteredList.length >= 3
                    ? WrapAlignment.center
                    : WrapAlignment.start,
                runSpacing: 50,
                spacing: 50,
                children: [
                  for (var i = 0; i < controller.filteredList.length; i++)
                    cardElement(controller.filteredList[i])
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget cardElement(TrainingWorkshop trainingWorkshop) {
    RxDouble colorOpacityTraining = 0.45.obs;
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      onEnter: (val) {
        colorOpacityTraining.value = 0.2;
      },
      onExit: (val) {
        colorOpacityTraining.value = 0.45;
      },
      child: GestureDetector(
        onTap: () {
          _launchURL("${trainingWorkshop.url}");
        },
        child: Obx(
          () => Container(
            height: 250,
            width: 350,
            decoration: BoxDecoration(
              color: kSecondaryColor.withOpacity(0.8),
              image: DecorationImage(
                colorFilter: ColorFilter.mode(
                    kSecondaryColor.withOpacity(colorOpacityTraining.value),
                    BlendMode.dstATop),
                fit: BoxFit.cover,
                image: NetworkImage(
                  trainingWorkshop.imageLink,
                ),
              ),
            ),
            child: Stack(
              children: [
                Positioned(
                  top: 20,
                  left: -35,
                  child: ClipPath(
                      clipper: NavBarClipper(),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(50, 10, 50, 10),
                        width: 350,
                        color: kSecondaryColor.withOpacity(0.8),
                        child: Text(trainingWorkshop.name, style: kTextStyle.copyWith(color: Colors.black),),
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
