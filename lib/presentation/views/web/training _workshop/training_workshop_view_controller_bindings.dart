
import 'package:cherchernom/presentation/views/web/training%20_workshop/training_workshop_view_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';

class TrainingWorkshopViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => TrainingWorkshopViewController(
      ),
    );
  }
}
