import 'package:cherchernom/domain/feature/training_code/controller/training_code_controller.dart';
import 'package:cherchernom/domain/feature/training_code/entities/training_code.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/styles/styles.dart';

class TrainingCodeViewController extends GetxController
    with StateMixin, SingleGetTickerProviderMixin {
  final RxInt currentIndex = 0.obs;

  GlobalKey test = GlobalKey();

  Rx<TextStyle> itemAppbarStyle =
      TextStyle(color: kTitleColorTransparent, fontSize: 20).obs;

  List<TrainingCode> listTrainingCode =
      TrainingCodeController().listTrainingCode;

  RxList<TrainingCode> filteredList = <TrainingCode>[].obs;

  String nameFilter = '';

  ThemeTrainingCode themeTrainingCode = ThemeTrainingCode.All;


  @override
  void onInit() async {
    change(null, status: RxStatus.loading());
    filteredList.addAll(listTrainingCode);
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void filterList() {
    filteredList.clear();
    listTrainingCode.forEach((element) {
      if (element.name.toLowerCase().contains(nameFilter.toLowerCase())&& (themeTrainingCode == ThemeTrainingCode.All || element.theme == themeTrainingCode)) {
        filteredList.add(element);
      }
    });
  }
}
