import 'package:cherchernom/domain/feature/training_code/entities/training_code.dart';
import 'package:cherchernom/presentation/core/clip/nav_bar_clipper.dart';
import 'package:cherchernom/presentation/core/clip/training_code_clipper.dart';
import 'package:cherchernom/presentation/core/styles/styles.dart';
import 'package:cherchernom/presentation/core/widgets/dropdown_multi.dart';
import 'package:cherchernom/presentation/core/widgets/n_button.dart';
import 'package:cherchernom/presentation/navigation/routes.dart';
import 'package:cherchernom/presentation/scaffold/loading_scaffold.dart';
import 'package:cherchernom/presentation/views/web/training_code/training_code_view_controller.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../scaffold/scaffold_view.dart';

class TrainingCodeView extends GetView<TrainingCodeViewController> {
  TrainingCodeView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onEmpty: Text(
        "Empty",
        style: TextStyle(color: Colors.black),
      ),
      onLoading: LoadingScaffold(),
      onError: (e) {
        print(e);
        return LinearProgressIndicator();
      },
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      isHome: false,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(100, 25, 50, 25),
            child: Row(
              children: [
                Container(
                  height: 50,
                  width: 400,
                  child: TextField(
                    onChanged: (value) {
                      controller.nameFilter = value;
                      controller.filterList();
                    },
                    decoration: InputDecoration(
                        hintText: "Entrez un nom d'entrainement",
                        hintStyle: kTextStyle.copyWith(
                            color: Colors.black, fontSize: 20)),
                  ),
                ),
                SizedBox(
                  width: 50,
                ),
                Container(
                    width: 400,
                    height: 50,
                    child: DropdownSearch(
                      popupBackgroundColor: kSecondaryColor.withOpacity(0.7),
                      onChanged: (value) {
                        controller.themeTrainingCode = value;
                        controller.filterList();
                      },
                      dropdownSearchDecoration: InputDecoration(
                        hintText: 'Tous',
                        hintStyle: kTextStyle.copyWith(
                            color: Colors.black, fontSize: 20),
                      ),
                      items: [
                        ThemeTrainingCode.All,
                        ThemeTrainingCode.Attaque,
                        ThemeTrainingCode.Defense,
                      ],
                      itemAsString: (val) =>
                          val.toString().replaceAll('ThemeTrainingCode.', ''),
                      mode: Mode.MENU,
                    )),
              ],
            ),
          ),
          Container(
            // color: Colors.red,
            padding: EdgeInsets.only(left: 50, right: 50),
            width: Get.width,
            child: Obx(() => Wrap(
                  alignment: controller.filteredList.length >= 3
                      ? WrapAlignment.center
                      : WrapAlignment.start,
                  key: controller.test,
                  children: [
                    for (var i = 0; i < controller.filteredList.length; i++)
                      element(controller.filteredList[i]),
                  ],
                )),
          ),
        ],
      ),
    );
  }

  Widget element(TrainingCode trainingCode) {
    return ClipPath(
      clipper: TrainingCodeClipper(),
      child: Container(
        width: 500,
        margin: EdgeInsets.all(25),
        padding: EdgeInsets.only(left: 70, top: 15, bottom: 15),
        decoration: BoxDecoration(
          // borderRadius:BorderRadius.all( Radius.circular(30)) ,
          color: kSecondaryColor,
          image: DecorationImage(
            colorFilter: ColorFilter.mode(
                kSecondaryColor.withOpacity(0.45), BlendMode.dstATop),
            fit: BoxFit.cover,
            image: AssetImage(
              'assets/background/banniere_wait.png',
            ),
          ),
        ),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 300,
                  child: Text(
                    trainingCode.name,
                    style: kCardTextStyle,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                  width: 300,
                  child: SelectableText(
                    trainingCode.value,
                  ),
                ),
              ],
            ),
            SizedBox(
              width: 25,
            ),
            InkWell(
                onTap: () {
                  Clipboard.setData(ClipboardData(text: trainingCode.value));
                  // showMenu(context: Get.context, position: position, items: items)
                },
                child: Icon(
                  Icons.copy,
                  size: 35,
                  color: Colors.white,
                )),
          ],
        ),
      ),
    );
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
