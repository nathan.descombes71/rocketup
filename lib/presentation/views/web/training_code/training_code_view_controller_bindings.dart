
import 'package:cherchernom/presentation/views/web/training_code/training_code_view_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';

class TrainingCodeViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => TrainingCodeViewController(
      ),
    );
  }
}
