
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'training_view_controller.dart';

class TrainingViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => TrainingViewController(
      ),
    );
  }
}
