import 'package:cherchernom/presentation/core/clip/nav_bar_clipper.dart';
import 'package:cherchernom/presentation/core/styles/styles.dart';
import 'package:cherchernom/presentation/core/widgets/n_button.dart';
import 'package:cherchernom/presentation/navigation/routes.dart';
import 'package:cherchernom/presentation/scaffold/loading_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../scaffold/scaffold_view.dart';
import 'training_view_controller.dart';

class TrainingView extends GetView<TrainingViewController> {
  TrainingView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onEmpty: Text(
        "Empty",
        style: TextStyle(color: Colors.black),
      ),
      onLoading: LoadingScaffold(),
      onError: (e) {
        print(e);
        return LinearProgressIndicator();
      },
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      isHome: false,
      body: Center(
        child: Padding(
          padding: EdgeInsets.only(top: 50, bottom: 50),
          child: Obx(() => Wrap(
                runSpacing: 50,
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 50,
                  ),
                  card(
                      "WORKSHOP",
                      "Quelques liens pour découvrir les maps de la communauté !",
                      controller.indexHover.value == 1,
                      1,
                      Routes.TRAININGWORKSHOP),
                  SizedBox(
                    width: 50,
                  ),
                  card(
                      "TRAINING CODE",
                      "Des codes d'entrainements choisis pas des coachs juste pour vous !",
                      controller.indexHover.value == 2,
                      2,
                      Routes.TRAININGCODE),
                  SizedBox(
                    width: 50,
                  ),
                  card(
                      "VIDEO TUTO",
                      "Des vidéos pour vous apprendre les meilleurss mécaniques de Rocket League",
                      controller.indexHover.value == 3,
                      3,
                      Routes.TRAININGVIDEO),
                  SizedBox(
                    width: 50,
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget card(
      String title, String desc, bool isHover, int index, String routeName) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      onEnter: (val) {
        controller.indexHover.value = index;
      },
      onExit: (val) {
        controller.indexHover.value = 0;
      },
      child: GestureDetector(
        onTap: () {
          Get.toNamed(routeName);
        },
        child: Stack(
          children: [
            ClipPath(
                clipper: NavBarClipper(),
                child: Container(
                  // padding: EdgeInsets.only(left: 45, top: 10),
                  height: 300,
                  width: 500,
                  decoration: BoxDecoration(
                    color: kSecondaryColor,
                    image: DecorationImage(
                      colorFilter: ColorFilter.mode(
                          kSecondaryColor.withOpacity(isHover ? 0.2 : 0.45),
                          BlendMode.dstATop),
                      fit: BoxFit.cover,
                      image: AssetImage(
                        'assets/background/banniere_wait.png',
                      ),
                    ),
                  ),
                  child: Stack(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 65, top: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AnimatedDefaultTextStyle(
                              child: Text(title),
                              style: kCardTextStyle.copyWith(
                                  fontSize: isHover ? 40 : 35),
                              duration: Duration(milliseconds: 300),
                            ),
                            // isHover?Text("Aller sur cette page"): SizedBox(),
                          ],
                        ),
                      ),
                      AnimatedPositioned(
                          curve: Curves.decelerate,
                          top: isHover ? 300 : 60,
                          left: -10,
                          child: ClipPath(
                            clipper: NavBarClipper(),
                            child: Container(
                              padding:
                                  EdgeInsets.only(left: 75, right: 20, top: 20),
                              height: 256,
                              width: 400,
                              color: kSecondaryColor.withOpacity(0.5),
                              child: Text(
                                desc,
                                style: kTextStyle.copyWith(fontSize: 25),
                              ),
                            ),
                          ),
                          duration: Duration(milliseconds: 300))
                    ],
                  ),
                )),
            Positioned(
              right: 0,
              top: 125,
              child: AnimatedContainer(
                duration: Duration(milliseconds: 200),
                height: isHover ? 55 : 50,
                width: isHover ? 55 : 50,
                color: kSecondaryColor,
                child: Icon(
                  Icons.login,
                  size: isHover ? 40 : 35,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
