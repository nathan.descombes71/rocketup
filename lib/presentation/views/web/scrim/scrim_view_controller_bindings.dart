
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'scrim_view_controller.dart';

class ScrimViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => ScrimViewController(
      ),
    );
  }
}
