
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'login_view_controller.dart';

class LoginViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => LoginViewController(
      ),
    );
  }
}
