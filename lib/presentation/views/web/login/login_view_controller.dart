import 'package:cherchernom/presentation/core/widgets/n_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/styles/styles.dart';

class LoginViewController extends GetxController
    with StateMixin, SingleGetTickerProviderMixin {
  final RxInt currentIndex = 0.obs;

  Rx<TextStyle> itemAppbarStyle =
      TextStyle(color: kTitleColorTransparent, fontSize: 20).obs;

  Color colorLogo = Colors.white;

  String textOnButton;

  double rightMargin1 = 0.0;
  double opacityFormPannel = 1.0;
  double opacityText = 1.0;

  double heightPannel = Get.width * 0.35;

  Alignment alignPannel1 = Alignment.centerLeft;

  double rightMargin2 = 0.0;

  Rx<Widget> widgetToShow;

  Alignment alignPannel2 = Alignment.centerRight;

  bool animeThisText = false;

  String titleTextContainer = 'Pas encors inscrit ?';
  String descTextContainer = 'Inscrivez-vous en cliquant sur ce bouton !';

  bool connectIsShowed = true;

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());
    widgetToShow = formConnect(Get.context).obs;
    textOnButton = 'S\'ENREGISTRER';
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void animationHeight() async {
    heightPannel = Get.width * 0.7;
    refresh();
    await Future.delayed(Duration(milliseconds: 400), () {
      heightPannel = Get.width * 0.35;
    });
    refresh();
  }

  void changeColor() async {
    if (rightMargin1 != 0) {
      colorLogo = kPrimaryColor;
    } else {
      await Future.delayed(Duration(milliseconds: 750), () {});
      colorLogo = Colors.white;
    }
    refresh();
  }

  void changeOpacity() async {
    opacityFormPannel = 0.0;
    opacityText = 0.0;
    await Future.delayed(Duration(milliseconds: 500), () {});
    opacityText = 1.0;
    opacityFormPannel = 1.0;
    refresh();
  }

  @override
  void refresh() {
    super.refresh();
  }

  Future<void> changeWidget() async {
    await Future.delayed(Duration(milliseconds: 400), () {});
    if (connectIsShowed) {
      widgetToShow.value = formRegister(Get.context);
      textOnButton = 'SE CONNECTER';
      connectIsShowed = false;
    } else {
      widgetToShow.value = formConnect(Get.context);
      textOnButton = 'S\'ENREGISTRER';
      connectIsShowed = true;
    }
    refresh();
  }

  Future<void> animateText() async {
    animeThisText = true;
    await Future.delayed(
      Duration(milliseconds: 500),
    );
    if (alignPannel2 == Alignment.centerLeft) {
      titleTextContainer = 'Déjà inscrit ?';
      descTextContainer = 'Connectez-vous en cliquant sur ce bouton !';
    } else {
      titleTextContainer = 'Pas encors inscrit ?';
      descTextContainer = 'Inscrivez-vous en cliquant sur ce bouton !';
    }
    await Future.delayed(
      Duration(milliseconds: 300),
    );
    animeThisText = false;
  }

  Widget formConnect(BuildContext bc) {
    return Container(
      margin: EdgeInsets.only(
        top: bc.height * 0.25,
      ),
      constraints: BoxConstraints(maxWidth: bc.width * 0.5),
      child: Column(
        children: [
          SelectableText(
            'Connexion à votre compte',
            style: TextStyle(
                color: Colors.purple,
                fontSize: 48,
                fontWeight: FontWeight.bold),
          ),
          SelectableText(
            'Via les réseaux sociaux',
            style: TextStyle(
                color: Colors.purple,
                fontSize: 25,
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.w100),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IntrinsicWidth(
                child: InkWell(
                  onTap: () {},
                  child: Card(
                    elevation: 15,
                    child: Container(
                      margin: EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Image.network(
                            'assets/logo/google.png',
                            height: 20,
                            width: 20,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Text(
                            'Continuer avec Google',
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              IntrinsicWidth(
                child: InkWell(
                  onTap: () {},
                  child: Card(
                    elevation: 15,
                    child: Container(
                      margin: EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Image.network(
                            'assets/logo/facebook.png',
                            height: 20,
                            width: 20,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Text(
                            'Continuer avec Google',
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 35,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(right: 10),
                height: 1,
                width: Get.width * 0.1,
                color: kPrimaryColor,
              ),
              SelectableText(
                'OU',
                style: TextStyle(color: kPrimaryColor),
              ),
              Container(
                margin: EdgeInsets.only(left: 10),
                height: 1,
                width: Get.width * 0.1,
                color: kPrimaryColor,
              )
            ],
          ),
          Container(
              margin: EdgeInsets.only(bottom: 20, top: 35),
              width: 350,
              child: TextFormField(
                cursorColor: kPrimaryColor,
                decoration: InputDecoration(
                  hintText: 'Mail/pseudo',
                  hintStyle: TextStyle(color: Colors.black.withOpacity(0.3)),
                ),
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20),
              width: 350,
              child: TextFormField(
                obscureText: true,
                cursorColor: kPrimaryColor,
                decoration: InputDecoration(
                  focusColor: Colors.blue,
                  filled: true,
                  hintText: 'Mot de passe',
                  hintStyle: TextStyle(color: Colors.black.withOpacity(0.3)),
                ),
              )),
              NButton(
            isSimpleButton: true,
            backgroundColor: Color(0xFF86229f),
            onPressed: () {},
            padding: EdgeInsets.fromLTRB(15,10, 15, 10),
            textWidget: Text(
              'CONNEXION',
              style: kTextStyle.copyWith(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  Widget formRegister(BuildContext bc) {
    return Container(
      margin: EdgeInsets.only(
        top: bc.height * 0.25,
      ),
      constraints: BoxConstraints(minWidth: 1000),
      child: Column(
        children: [
          SelectableText(
            'Inscrivez vous pour continuer',
            style: TextStyle(
                color: Colors.purple,
                fontSize: 48,
                fontWeight: FontWeight.bold),
          ),
          SelectableText(
            'Via les réseaux sociaux',
            style: TextStyle(
                color: Colors.purple,
                fontSize: 25,
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.w100),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IntrinsicWidth(
                child: InkWell(
                  onTap: () {},
                  child: Card(
                    elevation: 15,
                    child: Container(
                      margin: EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Image.network(
                            'assets/logo/google.png',
                            height: 20,
                            width: 20,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Text(
                            'Continuer avec Google',
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              IntrinsicWidth(
                child: InkWell(
                  onTap: () {},
                  child: Card(
                    elevation: 15,
                    child: Container(
                      margin: EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Image.network(
                            'assets/logo/facebook.png',
                            height: 20,
                            width: 20,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Text(
                            'Continuer avec Google',
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 35,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(right: 10),
                height: 1,
                width: Get.width * 0.1,
                color: kPrimaryColor,
              ),
              SelectableText(
                'OU',
                style: TextStyle(color: kPrimaryColor),
              ),
              Container(
                margin: EdgeInsets.only(left: 10),
                height: 1,
                width: Get.width * 0.1,
                color: kPrimaryColor,
              )
            ],
          ),
          SizedBox(
            height: 35,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  margin: EdgeInsets.only(bottom: 20),
                  width: 350,
                  child: TextFormField(
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                      hintText: 'Nom',
                      hintStyle:
                          TextStyle(color: Colors.black.withOpacity(0.3)),
                    ),
                  )),
              SizedBox(
                width: 15,
              ),
              Container(
                  margin: EdgeInsets.only(bottom: 20),
                  width: 350,
                  child: TextFormField(
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                      hintText: 'Prénom',
                      hintStyle:
                          TextStyle(color: Colors.black.withOpacity(0.3)),
                    ),
                  )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  margin: EdgeInsets.only(bottom: 20),
                  width: 350,
                  child: TextFormField(
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                      hintText: 'Mail/pseudo',
                      hintStyle:
                          TextStyle(color: Colors.black.withOpacity(0.3)),
                    ),
                  )),
              SizedBox(
                width: 15,
              ),
              Container(
                  margin: EdgeInsets.only(bottom: 20),
                  width: 350,
                  child: TextFormField(
                    obscureText: true,
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                      focusColor: Colors.blue,
                      filled: true,
                      hintText: 'Mot de passe',
                      hintStyle:
                          TextStyle(color: Colors.black.withOpacity(0.3)),
                    ),
                  )),
            ],
          ),
          NButton(
            isSimpleButton: true,
            backgroundColor: Color(0xFF86229f),
            onPressed: () {},
            padding: EdgeInsets.fromLTRB(15,10, 15, 10),
            textWidget: Text(
              'M\'INSCRIRE',
              style: kTextStyle.copyWith(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
