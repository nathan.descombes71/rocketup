import 'package:cherchernom/presentation/core/styles/styles.dart';
import 'package:cherchernom/presentation/core/widgets/animated_drawer.dart';
import 'package:cherchernom/presentation/core/widgets/show_up.dart';
import 'package:cherchernom/presentation/core/widgets/n_button.dart';
import 'package:cherchernom/presentation/scaffold/loading_scaffold.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../scaffold/scaffold_view.dart';
import 'login_view_controller.dart';

class LoginView extends GetView<LoginViewController> {
  LoginView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onEmpty: LoadingScaffold(),
      onLoading: LoadingScaffold(),
      onError: (e) {
        print(e);
        return LinearProgressIndicator();
      },
    );
  }

  Widget buildContent(BuildContext context) {
    return Scaffold(
      endDrawer: Builder(
        builder: (bContext) => drawer(bContext),
      ),
      body: Container(
        //  decoration: BoxDecoration(image: DecorationImage(fit: BoxFit.cover, image:  NetworkImage('assets/background/backgroundloginregister.png'),),),
        child: LayoutBuilder(builder: (lBContext, lBconstraints) {
          print(lBContext.width);
          if (lBContext.width >= 1340) {
            return largeScreen(lBContext);
          } else {
            return mediumScreen(lBContext);
          }
        }),
      ),
    );
  }

  Widget largeScreen(BuildContext lBContext) {
    return Stack(
      children: [
        AnimatedAlign(
          curve: Curves.ease,
          child: Container(
            height: Get.height,
            width: Get.width * 0.7,
            color: Colors.white,
            child: Obx(
              () => AnimatedOpacity(
                  opacity: controller.opacityFormPannel,
                  duration: Duration(milliseconds: 500),
                  child: controller.widgetToShow.value),
            ),
          ),
          duration: Duration(milliseconds: 1000),
          alignment: controller.alignPannel2,
        ),
        AnimatedAlign(
          curve: Curves.ease,
          child: AnimatedContainer(
            curve: Curves.easeInOutQuad,
            duration: Duration(milliseconds: 670),
            height: Get.height,
            width: controller.heightPannel,
            // color: Color(0xFF86229f),
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fitHeight,
                image: AssetImage(
                    'assets/background/backgroundloginregister.png'),
              ),
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ShowUp(
                    reverseAnim: controller.animeThisText,
                    delay: 200,
                    child: Container(
                      child: Column(
                        children: [
                          Text(
                            controller.titleTextContainer.toUpperCase(),
                            style: kTitleStyle.copyWith(
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.normal),
                          ),
                          SizedBox(
                            height: 25,
                          ),
                          Container(
                            width: 300,
                            child: Text(
                              controller.descTextContainer,
                              textAlign: TextAlign.center,
                              style: kTextStyle.copyWith(
                                  fontSize: 24, color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 45,
                  ),
                  NButton(
                    isSimpleButton: true,
                    // backgroundColor: Color(0xFFd6a4e0),
                    textWidget: AnimatedOpacity(
                      duration: Duration(milliseconds: 500),
                      opacity: controller.opacityText,
                      child: Text(
                        controller.textOnButton,
                        style: kTextStyle.copyWith(color: Colors.black),
                      ),
                    ),
                    onPressed: () async {
                      controller.animationHeight();
                      if (controller.rightMargin1 == 0) {
                        controller.rightMargin1 = lBContext.width * 0.7;
                      } else {
                        controller.rightMargin1 = 0;
                      }
                      if (controller.alignPannel1 == Alignment.centerLeft) {
                        controller.alignPannel1 = Alignment.centerRight;
                      } else {
                        controller.alignPannel1 = Alignment.centerLeft;
                      }

                      if (controller.rightMargin2 == 0) {
                        controller.rightMargin2 = lBContext.width * 0.7;
                      } else {
                        controller.rightMargin2 = 0;
                      }
                      if (controller.alignPannel2 == Alignment.centerLeft) {
                        controller.alignPannel2 = Alignment.centerRight;
                      } else {
                        controller.alignPannel2 = Alignment.centerLeft;
                      }

                      controller.changeOpacity();
                      controller.changeColor();
                      controller.changeWidget();
                      await controller.animateText();
                      controller.refresh();
                    },
                    padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                  ),
                ],
              ),
            ),
          ),
          duration: Duration(milliseconds: 1200),
          alignment: controller.alignPannel1,
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Container(
            margin: EdgeInsets.fromLTRB(50, 40, 0, 0),
            child: Image.network(
              'assets/logo/logoNonOffi.png',
              height: 30,
              color: controller.colorLogo,
            ),
          ),
        ),
        Positioned(
            top: 20,
            right: 50,
            child: AnimatedDrawer(
              size: 40,
              onPressed: () {
                Scaffold.of(lBContext).openEndDrawer();
              },
              scaffoldContext: lBContext,
            )),
      ],
    );
  }

  Widget mediumScreen(lBContext) {
    return Stack(
      children: [
        Align(
          alignment: Alignment.topLeft,
          child: Container(
            margin: EdgeInsets.fromLTRB(50, 40, 0, 0),
            child: Image.network(
              'assets/logo/logoNonOffi.png',
              height: 30,
              color: kPrimaryColor,
            ),
          ),
        ),
        AnimatedOpacity(
            duration: Duration(milliseconds: 200),
            opacity: controller.opacityFormPannel,
            child: Column(
              children: [
                Center(child: controller.widgetToShow.value),
                SizedBox(height: 15,),
                InkWell(
                  child: Text(controller.connectIsShowed
                      ? 'Pas encore inscrit ? Clique ici !'
                      : 'Déjà un compte ? Clique ici !'),
                  onTap: () async {
                    controller.changeOpacity();
                    // controller.changeColor();
                    controller.changeWidget();
                    // await controller.animateText();
                    controller.refresh();
                  },
                ),
              ],
            )),

        // NButton(
        //   isSimpleButton: true,
        //   textWidget: Text('Changez machin'),
        //   onPressed: () async {
        //     controller.changeOpacity();
        //     // controller.changeColor();
        //     controller.changeWidget();
        //     // await controller.animateText();
        //     controller.refresh();
        //   },
        //   backgroundColor: Colors.black,
        // ),
        Positioned(
            top: 20,
            right: 50,
            child: AnimatedDrawer(
              size: 40,
              onPressed: () {
                Scaffold.of(lBContext).openEndDrawer();
              },
              scaffoldContext: lBContext,
            )),
      ],
    );
  }

  Widget drawer(BuildContext bContext) {
    return Container(
      padding: EdgeInsets.only(top: 15),
      color: Colors.white,
      width: 500,
      height: Get.size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 15,
              ),
              AnimatedDrawer(
                size: 40,
                scaffoldContext: bContext,
                onPressed: () {},
              ),
              SizedBox(
                width: 50,
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(top: 50),
                  child: Row(
                    children: [
                      Image.network(
                        'assets/logo/logo_appli.png',
                        height: 80,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        child: SelectableText(
                          "Téléchargez notre application pour résérver de n'importe où !",
                          style: kTextStyle.copyWith(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              NButton(
                onPressed: () {},
                backgroundColor: kPrimaryColor,
                isSimpleButton: true,
                textWidget: Row(
                  children: [
                    Icon(Icons.android),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Android',
                      style: kTextStyle.copyWith(color: Colors.white),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 20,
              ),
              NButton(
                onPressed: () {},
                backgroundColor: kPrimaryColor,
                isSimpleButton: true,
                textWidget: Row(
                  children: [
                    Icon(Icons.phone_iphone),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Apple',
                      style: kTextStyle.copyWith(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 50,
          ),
          buttonReturnMenu(),
        ],
      ),
    );
  }

  Widget buttonReturnMenu() {
    return InkWell(
      onTap: () {
        debugPrint('return Menu');
      },
      child: Column(
        children: [
          Divider(
            color: Colors.black.withOpacity(0.5),
            height: 1,
            thickness: 1,
          ),
          SizedBox(
            height: 10,
          ),
          Center(
              child: Text(
            'Revenir au menu principal',
            style: kTextStyle.copyWith(
                color: kPrimaryColor,
                fontSize: 20,
                fontWeight: FontWeight.bold),
          )),
          SizedBox(
            height: 10,
          ),
          Divider(
              color: Colors.black.withOpacity(0.5), height: 1, thickness: 1),
        ],
      ),
    );
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
