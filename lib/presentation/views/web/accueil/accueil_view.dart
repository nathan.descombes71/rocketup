import 'package:cherchernom/presentation/core/clip/nav_bar_clipper.dart';
import 'package:cherchernom/presentation/core/styles/styles.dart';
import 'package:cherchernom/presentation/core/widgets/n_button.dart';
import 'package:cherchernom/presentation/navigation/routes.dart';
import 'package:cherchernom/presentation/scaffold/loading_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../scaffold/scaffold_view.dart';
import 'accueil_view_controller.dart';

class AccueilView extends GetView<AccueilViewController> {
  AccueilView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onEmpty: Text(
        "Empty",
        style: TextStyle(color: Colors.black),
      ),
      onLoading: LoadingScaffold(),
      onError: (e) {
        print(e);
        return LinearProgressIndicator();
      },
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      isHome: true,
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Container(
              // color: Colors.red,
              child: MouseRegion(
                cursor: SystemMouseCursors.click,
                onEnter: (value) {
                  controller.colorOpacityTraining.value = 0.2;
                   controller.sizeContainer.value = 55;
                },
                onExit: (value) {
                  controller.colorOpacityTraining.value = 0.45;
                  controller.sizeContainer.value = 50;
                },
                child: GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.TRAINING);
                  },
                  child: Stack(
                    children: [
                      ClipPath(
                        clipper: NavBarClipper(),
                        child: Obx(
                          () => Container(
                            padding: EdgeInsets.only(left: 130, top: 25),
                            width: 1200,
                            height: 300,
                            decoration: BoxDecoration(
                              color: kSecondaryColor.withOpacity(0.8),
                              image: DecorationImage(
                                colorFilter: ColorFilter.mode(
                                    kSecondaryColor.withOpacity(
                                        controller.colorOpacityTraining.value),
                                    BlendMode.dstATop),
                                fit: BoxFit.cover,
                                image: AssetImage(
                                  'assets/background/banniere_wait.png',
                                ),
                              ),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "TRAINING",
                                  style: kCardTitleStyle,
                                ),
                                Text(
                                  "Découvre une nouvelle façon d'apprendre",
                                  style: kSubtitle,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                          right: 35,
                          top: 125,
                          child: Obx(() => AnimatedContainer(
                            duration: Duration(milliseconds: 200),
                                height: controller.sizeContainer.value,
                                width: controller.sizeContainer.value,
                                color: kSecondaryColor,
                                child: Icon(
                                  Icons.login,
                                  size: 35,
                                ),
                              ),),),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 100,
            ),
          ],
        ),
      ),
    );
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
