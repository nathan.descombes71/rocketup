
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';
import 'accueil_view_controller.dart';

class AccueilViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => AccueilViewController(
      ),
    );
  }
}
