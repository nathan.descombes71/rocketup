
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/styles/styles.dart';

class AccueilViewController extends GetxController
    with StateMixin, SingleGetTickerProviderMixin {


  final RxInt currentIndex = 0.obs;

  Rx<TextStyle> itemAppbarStyle =
      TextStyle(color: kTitleColorTransparent, fontSize: 20).obs;
  
  RxDouble colorOpacityTraining = 0.45.obs;

  RxDouble sizeContainer = 50.0.obs;

  @override
  void onInit() async {

    change(
      null,
      status: RxStatus.loading()
    );
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }
}
