import 'package:cherchernom/domain/feature/training_video/controller/training_video_controller.dart';
import 'package:cherchernom/domain/feature/training_video/entities/training_video.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/styles/styles.dart';

class TrainingVideoViewController extends GetxController
    with StateMixin, SingleGetTickerProviderMixin {
  final RxInt currentIndex = 0.obs;

  List<TrainingVideo> listTrainingVideo =
      TrainingVideoController().listTrainingVideo;

  Rx<TextStyle> itemAppbarStyle =
      TextStyle(color: kTitleColorTransparent, fontSize: 20).obs;

  RxList<TrainingVideo> filteredList = <TrainingVideo>[].obs;

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());

    filteredList.addAll(listTrainingVideo);
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }
}
