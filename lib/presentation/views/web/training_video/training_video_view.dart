import 'package:cherchernom/domain/feature/training_video/entities/training_video.dart';
import 'package:cherchernom/presentation/core/extensions/color_extensions.dart';
import 'package:cherchernom/presentation/core/styles/styles.dart';
import 'package:cherchernom/presentation/core/widgets/n_button.dart';
import 'package:cherchernom/presentation/core/widgets/video_tuto.dart';
import 'package:cherchernom/presentation/navigation/routes.dart';
import 'package:cherchernom/presentation/scaffold/loading_scaffold.dart';
import 'package:cherchernom/presentation/views/web/training_video/training_video_view_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../scaffold/scaffold_view.dart';

class TrainingVideoView extends GetView<TrainingVideoViewController> {
  TrainingVideoView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
      onEmpty: Text(
        "Empty",
        style: TextStyle(color: Colors.black),
      ),
      onLoading: LoadingScaffold(),
      onError: (e) {
        print(e);
        return LinearProgressIndicator();
      },
    );
  }

  Widget buildContent(BuildContext context) {
    return ScaffoldView(
      isHome: false,
      body: Center(
        child: Wrap(
          alignment: controller.filteredList.length >= 3
              ? WrapAlignment.center
              : WrapAlignment.start,
          spacing: 50,
          children: [
            for (var i = 0; i < controller.filteredList.length; i++)
              cardElement(controller.filteredList[i]),
          ],
        ),
      ),
    );
  }

  Widget cardElement(TrainingVideo actualElement) {
    return InkWell(
      onTap: () {},
      child: Container(
        height: 300,
        width: 300,
        color: kSecondaryColor,
        child: Column(
          children: [
            SizedBox(height: 20,),
            Text(
              actualElement.nameVideo,
              style: kCardTextStyle.copyWith(color: Colors.black, fontSize: 30),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 10, 20, 25),
                child: Text(
              "consiste en l'utilisation du salto arrière de la voiture afin de propulser la balle vers l'avant plus rapidement.",
              style: kTextStyle.copyWith(fontSize: 20, color: Colors.black),
            )),
            Container(
              padding: EdgeInsets.only(left: 15),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    height: 53,
                    width: 53,
                    decoration: BoxDecoration(
                        color: HexColor.colorConvert(actualElement.hexaColor),
                        shape: BoxShape.circle),
                    child: Center(
                      child: Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              actualElement.linkPDP,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Text(
                    actualElement.author,
                    style: kTextStyle.copyWith(color: Colors.black),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
