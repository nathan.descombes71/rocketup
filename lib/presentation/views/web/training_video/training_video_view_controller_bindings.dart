
import 'package:cherchernom/presentation/views/web/training_video/training_video_view_controller.dart';
import 'package:get/get.dart';

// import '../views_mobile.exports.dart';

class TrainingVideoViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => TrainingVideoViewController(
      ),
    );
  }
}
