import 'dart:html';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class VideoPlease extends StatefulWidget {
  final Widget widget;
  // final Widget floatingActionButton;
  VideoPlease({
    Key key,
    this.widget,
  }) : super(key: key);

  @override
  _VideoPleaseState createState() => _VideoPleaseState();
}

class _VideoPleaseState extends State<VideoPlease>
    with TickerProviderStateMixin {
  YoutubePlayerController _controller;
  IFrameElement _iframeElement = IFrameElement();
  Widget _iframeWidget;
  @override
  void initState() {
    _iframeElement.height = '500';
    _iframeElement.width = '500';
    _iframeElement.src = 'https://www.youtube.com/embed/VoFNIaSoX-U';
    _iframeElement.style.border = 'none';

    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
      'iframeElement',
      (int viewId) => _iframeElement,
    );

    _iframeWidget = HtmlElementView(
      key: UniqueKey(),
      viewType: 'iframeElement',
    );
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: 'VoFNIaSoX-U', // passed in to the widget constructor
      params: YoutubePlayerParams(
        showControls: true,
        autoPlay: true,
        // mute: true,
        showFullscreenButton: true,
      ),
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
    };
    _controller.onExitFullscreen = () {};
  }

  @override
  Widget build(BuildContext context) {
    return YoutubePlayerControllerProvider(
      controller: _controller,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          YoutubePlayerIFrame(
            aspectRatio: 16 / 9,
          ),
          // SizedBox(
          //   height: 400,
          //   width: 400,
          //   child: _iframeWidget,
          // )
          // YoutubeValueBuilder(
          //   builder: (context, value) => ElevatedButton(
          //     child: const Text('Unmute'),
          //     onPressed: () => context.ytController.unMute(),
          //   ),
          // ),
          //<iframe width="1280" height="720" src="https://www.youtube.com/embed/HjNwpz3KinQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        ],
      ),
    );
  }
}
