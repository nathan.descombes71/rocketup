
import 'dart:ui';

import 'package:cherchernom/presentation/core/styles/styles.dart';
import 'package:cherchernom/presentation/core/widgets/show_up.dart';
import 'package:cherchernom/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class XTutorialMenu {
  static void show(BuildContext context,
      {Color backgroundColor, Function() onHide, double marginTop, List<ShowMenuItem> allItem, double marginRight}) {
    XTutorialMenuUtil.createView(
      context: context,
      child: XTutorialMenuBaseWidget(
        onHide: () {
          onHide();
          hide();
        },
        marginRight: marginRight,
        allItem: allItem,
        marginTop: marginTop,
        backgroundColor: backgroundColor,
        context: context,

      ),
    );
  }

  static void hide() {
    XTutorialMenuUtil.dismiss();
  }

  static get isVisible => XTutorialMenuUtil.isVisible;
}

class XTutorialMenuUtil {
  static OverlayState state;
  static OverlayEntry entry;
  static bool isVisible = false;

  static void createView({
    BuildContext context,
    Widget child,
  }) async {
    if (!isVisible) {
      dismiss();
      state = Overlay.of(context);
      entry = OverlayEntry(builder: (_) => child);
      isVisible = true;
      state.insert(entry);
    }
  }

  static void dismiss() async {
    if (!isVisible) {
      return;
    }

    isVisible = false;
    entry?.remove();
  }
}

class XTutorialMenuBaseWidget extends StatefulWidget {
  final Color backgroundColor;
  final VoidCallback onHide;
  final double blurPower;
  final BuildContext context;
  final double marginTop;
  final List<ShowMenuItem> allItem;
  final double marginRight;

  const XTutorialMenuBaseWidget({
    Key key,
    this.backgroundColor,
    this.onHide,
    this.blurPower,
    this.context,
    this.marginTop, this.allItem, this.marginRight,
  }) : super(key: key);

  @override
  __TDBaseWidgetState createState() => __TDBaseWidgetState();
}

class __TDBaseWidgetState extends State<XTutorialMenuBaseWidget>
    with SingleTickerProviderStateMixin {
  Duration animationDuration = Duration(milliseconds: 200);
  AnimationController scaleController;
  AnimationController animationController;
  Animation<double> scaleAnimation;
  Animation<double> fadeAnimation;

  int step = 0;
  bool reverseAnim = false;
  bool isClickable = true;

  bool oneTime = false;

  final Tween<double> scaleTween = Tween<double>(begin: 0.9, end: 1.0);
  final Tween<double> fadeTween = Tween<double>(begin: 0.0, end: 1.0);

  @override
  void initState() {
    initAnimations();
    super.initState();
  }

  Future<void> initAnimations() async {
    animationController = AnimationController(
      duration: Duration(milliseconds: 200),
      vsync: this,
    );
    scaleAnimation = scaleTween.animate(animationController);
    fadeAnimation = fadeTween.animate(animationController);
    animationController.forward();
    await Future.delayed(animationDuration);
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        width: 400,
        height: 400,
        child: GestureDetector(
          onTap: () {
            if (isClickable) {
              widget.onHide();
            }
          },
          child: LayoutBuilder(builder: (context, constraints) {
            return Container(
              width: 400,
              height: 400,
              child: buildBody(context),
            );
          }),
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return ScaleTransition(
      scale: scaleAnimation,
      child: FadeTransition(
        opacity: fadeAnimation,
        child: buildContent(context),
      ),
    );
  }

  Widget buildContent(BuildContext context) {
    double screenWidth =
        MediaQuery.of(context).orientation == Orientation.portrait
            ? MediaQuery.of(context).size.width
            : MediaQuery.of(context).size.height;

    return Container(
      width: 400,
      height: 400,
      child: Stack(
        children: [
          Positioned(
              top: 60-widget.marginTop,
              right: widget.marginRight,
              child: MouseRegion(
                cursor: SystemMouseCursors.click,
                onExit: (event) async {
                  await Future.delayed(Duration(milliseconds: 100), () {
                    widget.onHide();
                  });

                  // print(event.distance);
                },
                child: InkWell(
                  onTap: () {
                    Get.toNamed(Routes.TRAINING);
                  },
                  child: Container(
                    // color: kSecondaryColor,
                    // height: 200,
                    width: 230,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 140,
                        ),
                        Container(
                          color: kSecondaryColor,
                          width: 280,
                          child: Column(
                            children: [
                              for (var i = 0; i < widget.allItem.length; i++) 
                              InkWell(
                                onTap: () {
                                  Get.toNamed(widget.allItem[i].routes);
                                },
                                child: Container(
                                  padding: EdgeInsets.only(bottom: 10, top: 25),
                                  child: Center(
                                    child: Text(widget.allItem[i].title.toUpperCase(), style: kTextStyle.copyWith(color: Colors.black),),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ))
        ],
      ),
      alignment: Alignment.bottomCenter,
      decoration: BoxDecoration(
        color: getBackgroundColor(),
      ),
    );
  }

  // double getLeftPadding(TutorialItems tutorialItems){
  //   if(tutorialItems.positionDescription)
  // }

  Color getBackgroundColor() {
    // TODO not working
    if (widget.backgroundColor == null) {
      return Colors.transparent;
    } else {
      return Colors.transparent;
    }
  }
}

class ShowMenuItem {
  String title;
  String routes;

  ShowMenuItem({
    this.title,
    this.routes,
  });
}

extension onShowMenuItem on ShowMenuItem {
  ShowMenuItem copyWith({
    String title,
  String routes,
  }) {
    return ShowMenuItem(
      routes: routes,
      title: title,
    );
  }
}