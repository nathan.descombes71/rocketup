import 'package:flutter/material.dart';
import 'package:get/get.dart';

class XDropDownMulti<T> extends StatelessWidget {
  final ValueChanged<T> onChanged;
  final List<T> items;
  final String title;
  final RxList<T> itemsSelected;

  XDropDownMulti({
    Key key,
    @required this.onChanged,
    @required this.items,
    @required this.title,
    @required this.itemsSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async => await _openMenu(context),
      child: FormField<List<T>>(
        builder: (state) {
          return LayoutBuilder(
            builder: (_, constraint) => Container(
              alignment: Alignment.center,
              color: Get.theme.canvasColor,
              width: constraint.maxWidth,
              height: 30,
              child: Obx(
                () => Text(
                  itemsSelected.isNotEmpty
                      ? 'selectItem'.tr + ' ${itemsSelected.length}'
                      : title,
                  style: Get.theme.textTheme.bodyText2
                      .copyWith(color: Colors.grey),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Future _openMenu(BuildContext context) async {
    final RenderBox popupButtonObject = context.findRenderObject() as RenderBox;
    final RenderBox overlay =
        Overlay.of(context).context.findRenderObject() as RenderBox;
    return await showMenu(
      context: context,
      position: RelativeRect.fromSize(
        Rect.fromPoints(
          popupButtonObject.localToGlobal(
              popupButtonObject.size.bottomLeft(Offset.zero),
              ancestor: overlay),
          popupButtonObject.localToGlobal(
              popupButtonObject.size.bottomRight(Offset.zero),
              ancestor: overlay),
        ),
        Size(overlay.size.width, overlay.size.height),
      ),
      items: List.generate(
        items.length,
        (index) => XPopupMenuItem(
          child: InkWell(
            onTap: () {
              onChanged(items[index]);
            },
            child: Row(
              children: [
                Obx(
                  () => Checkbox(
                    value: itemsSelected.contains(items[index]) ||
                        itemsSelected.length == items.length - 1,
                    onChanged: (value) {
                      onChanged(items[index]);
                    },
                  ),
                ),
                Text(
                  '${items[index]}',
                  style: Get.theme.textTheme.bodyText2
                      .copyWith(color: Colors.grey),
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class XPopupMenuItem extends PopupMenuItem {
  XPopupMenuItem({
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  PopupMenuItemState<dynamic, PopupMenuItem> createState() =>
      XPopupMenuItemState();
}

class XPopupMenuItemState extends PopupMenuItemState {
  @override
  Widget build(BuildContext context) {
    TextStyle style = widget.textStyle ?? Theme.of(context).textTheme.subtitle1;
    Widget item = AnimatedDefaultTextStyle(
      style: style,
      duration: kThemeChangeDuration,
      child: Container(
        alignment: AlignmentDirectional.centerStart,
        constraints: BoxConstraints(minHeight: widget.height),
        padding: widget.padding ?? const EdgeInsets.symmetric(horizontal: 16),
        child: buildChild(),
      ),
    );
    return MergeSemantics(
      child: Semantics(
        enabled: widget.enabled,
        button: true,
        child: item,
      ),
    );
  }
}
