import 'dart:async';
import 'package:flutter/material.dart';

class AnimatedDrawer extends StatefulWidget {
  final Function() onPressed;
  final Color color;
  final BuildContext scaffoldContext;
  final double size;

  AnimatedDrawer({this.onPressed, this.color, this.scaffoldContext, this.size});

  @override
  _AnimatedDrawerState createState() => _AnimatedDrawerState();
}

class _AnimatedDrawerState extends State<AnimatedDrawer>
    with TickerProviderStateMixin {
  AnimationController _animController;
  bool isPlaying = false;

  @override
  void initState() {
    super.initState();

    _animController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    CurvedAnimation(curve: Curves.decelerate, parent: _animController);
    // _animOffset =
    //     Tween<Offset>(begin: const Offset(0.0, 0.35), end: Offset.zero)
    //         .animate(curve);
       if(Scaffold.of(widget.scaffoldContext).isEndDrawerOpen){
         _animController.forward();
       }else{
          _animController.reverse();
       }
  }

  void _handleOnPressed() {
    setState(() {
     if(Scaffold.of(widget.scaffoldContext).isEndDrawerOpen){
       _animController.forward();
       Navigator.pop(widget.scaffoldContext);
       _animController.dispose();
     }else{
        _animController.reverse();
     }
    });
  }

  // @override
  // void dispose() {
  //   super.dispose();
  //   _animController.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        size: widget.size,
        icon: AnimatedIcons.menu_close,
        progress: _animController,
      ),
      onPressed: () {
        _handleOnPressed();
        widget.onPressed();
      },
    );
  }
}
