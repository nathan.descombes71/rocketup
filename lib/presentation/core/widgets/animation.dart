import 'package:flutter/material.dart';

class AnimationWidget extends StatefulWidget {
  final Widget widget;
  // final Widget floatingActionButton;
  AnimationWidget({
    Key key,
    this.widget,
  }) : super(key: key);

  @override
  _AnimationWidgetState createState() => _AnimationWidgetState();
}

class _AnimationWidgetState extends State<AnimationWidget>
    with TickerProviderStateMixin {
  double opaValue = 0;

  @override
  void initState() {
    opaValue = 0;
    Future.delayed(Duration(seconds: 2), () {
      opaValue = 1;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: opaValue,
      duration: Duration(milliseconds: 200),
      child: widget,
    );
  }
}
