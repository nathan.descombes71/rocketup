import 'package:flutter/material.dart';
import 'package:cherchernom/presentation/core/styles/styles.dart';

class NButton extends StatefulWidget {
  final Function() onPressed;
  final String text;
  final Color backgroundColor;
  final EdgeInsets padding;
  final bool isSimpleButton;
  final Widget textWidget;
  final TextStyle style;
  // final Widget floatingActionButton;
  NButton({
    Key key,
    this.onPressed,
    this.text,
    this.backgroundColor,
    this.padding,
    this.isSimpleButton = false,
    this.textWidget, this.style,
  }) : super(key: key);

  @override
  _AnimationWidgetState createState() => _AnimationWidgetState();
}

class _AnimationWidgetState extends State<NButton>
    with TickerProviderStateMixin {
  double opaValue = 0;
  ButtonStyle thisButtonStyle = ButtonStyle();
  TextStyle thisTextStyle = TextStyle();

  @override
  void initState() {
    opaValue = 0;
    Future.delayed(Duration(seconds: 2), () {
      opaValue = 1;
    });

    thisTextStyle = kTextStyleSub.copyWith(color: Colors.white);
    thisButtonStyle = ButtonStyle(
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      )),
      backgroundColor: MaterialStateProperty.all<Color>(
          widget.backgroundColor ?? Color(0xfff47858f)),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onHover: (value) {
        setState(() {
          if (value) {
            thisButtonStyle = ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              )),
              backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
              side: MaterialStateProperty.all<BorderSide>(BorderSide(
                  color: widget.backgroundColor ?? Color(0xfff47858f),
                  width: 2)),
            );
            thisTextStyle = kTextStyleSub.copyWith(
                color: widget.backgroundColor ?? Color(0xfff47858f));
          } else {
            thisButtonStyle = ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              )),
              backgroundColor: MaterialStateProperty.all<Color>(
                  widget.backgroundColor ?? Color(0xfff47858f)),
            );
            thisTextStyle = kTextStyleSub.copyWith(color: Colors.white);
          }
        });
      },
      onTap: () {},
      child: !widget.isSimpleButton
          ? ElevatedButton(
              style: thisButtonStyle,
              onPressed: widget.onPressed,
              child: Padding(
                padding: widget.padding ?? const EdgeInsets.all(8.0),
                child: Text(
                  widget.text,
                  style: thisTextStyle,
                ),
              ))
          : ElevatedButton(
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                )),
                backgroundColor: MaterialStateProperty.all<Color>(widget.backgroundColor ?? Colors.white),
              ),
              onPressed: widget.onPressed,
              child: widget.text ==null ? Padding(
                padding: widget.padding ?? const EdgeInsets.all(8.0),
                child: widget.textWidget,
              ):Text(widget.text, style: widget.style,)),
    );
  }
}
