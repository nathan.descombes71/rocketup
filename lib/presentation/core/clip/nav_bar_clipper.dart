import 'package:flutter/material.dart';

class NavBarClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path
      ..lineTo(size.width, 0.0)
      ..lineTo(size.width * 0.90, size.height)
      ..lineTo(0, size.height)
      ..lineTo(size.width * 0.10, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(NavBarClipper oldClipper) => false;
}
