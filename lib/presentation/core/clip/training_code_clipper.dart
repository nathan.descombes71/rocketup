import 'package:flutter/material.dart';

class TrainingCodeClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path
      ..lineTo(size.width -40, 0.0)
      ..lineTo(size.width * 0.85, size.height)
      ..lineTo(40, size.height)
      ..lineTo(size.width * 0.15, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(TrainingCodeClipper oldClipper) => false;
}
