Map<String, String> get enUS {
  return const {
    //ACCUEIL
    'home': 'HOME',
    'lastNews': 'LAST NEWS',
    'about': 'ABOUT US',
    'thank': 'Thank you !',
    'allRightsReserved': 'all right reserved',
    'aboutusText':
        'The Abyss eSport is an eSports structure which aims to become one of the French leaders. Present on two games, Valorant and Rocket League, the Abyss eSport wants to perform to the maximum and train the best players in the world.',
    'anySponsor': 'Currently no sponsor',
    //CONTACT
    'lastName': 'Last name',
    'firstName': 'First name',
    'object': 'Object',
    'mail': 'e-mail',
    'yourMessage': 'Your message',
    'send': 'Send',
    'emailSentSuccess': 'Email sent successfully !',
    'becameSponsor': 'Become a sponsor',
    //NEWS
    'currentNews': 'CURRENT NEWS',
    'noNews': 'No news available for the moment',
    'writtenBy': 'Written by',
    'on': 'on',
    //Teams
    'seePlayersInfo': 'See players informations',
    'awards': 'AWARDS',
    'player': 'Player',
    'captain': 'Captain',
    //Error
    'objectFieldError': 'please fill in subject field',
    'emailFieldError': 'please fill in a valid email',
    'messageFieldError': 'please fill in message field',
  };
}
