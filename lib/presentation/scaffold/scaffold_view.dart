import 'package:cherchernom/presentation/core/clip/nav_bar_clipper.dart';
import 'package:cherchernom/presentation/core/widgets/show_up.dart';
import 'package:cherchernom/presentation/core/widgets/x_show_menu.dart';
import 'package:cherchernom/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'dart:js' as js;

import '../core/styles/styles.dart';
import 'scaffold_content/scaffold_triangle.dart';

class ScaffoldView extends StatefulWidget {
  final Widget body;
  final bool showScrollbar;
  final bool isCharged;
  final Widget drawer;
  final bool isHome;
  // final Widget floatingActionButton;
  ScaffoldView({
    Key key,
    this.body,
    this.isHome = false,
    this.showScrollbar = true,
    this.isCharged = true,
    this.drawer,
  }) : super(key: key);

  @override
  _ScaffoldViewState createState() => _ScaffoldViewState();
}

class _ScaffoldViewState extends State<ScaffoldView>
    with TickerProviderStateMixin {
  double rightPosNavBarSelected = 0;
  int index = 2;
  bool changePage = false;
  bool showMenuActif = false;
  ScrollController sscrollController = ScrollController();
  RxDouble marginTop = 0.0.obs;

  @override
  void initState() {
    initNavBar();
    sscrollController.addListener(() {
      marginTop.value = sscrollController.offset;
    });
    super.initState();
  }

  void initNavBar() {
    if (Get.currentRoute == Routes.ACCUEIL) {
      rightPosNavBarSelected = 738;
      index = 0;
    } else if (Get.currentRoute == Routes.SCRIM) {
      rightPosNavBarSelected = 538;
      index = 1;
    } else if (Get.currentRoute == Routes.TRAINING ||
        Get.currentRoute == Routes.TRAININGCODE ||
        Get.currentRoute == Routes.TRAININGWORKSHOP ||
        Get.currentRoute == Routes.TRAININGVIDEO) {
      rightPosNavBarSelected = 293;
      index = 2;
    } else if (Get.currentRoute == Routes.CONTACT) {
      rightPosNavBarSelected = 30;
      index = 3;
    }
  }

  @override
  Widget build(BuildContext context) {
    return buildContent(context);
  }

// Image.asset('assets/graph/banniere2.png'),
  Widget buildContent(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Scaffold(
          endDrawer: widget.drawer,
          body: SingleChildScrollView(
            controller: sscrollController,
            child: Column(
              children: [
                Container(
                  color: Colors.black,
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        child: Container(
                            child: Row(
                          children: [
                            Icon(
                              Icons.mail_outline_sharp,
                              color: kSecondaryColor,
                              size: 35,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'contact@rocket-up.fr',
                              style: kTextStyle.copyWith(
                                  color: Colors.white,
                                  fontStyle: FontStyle.italic),
                            ),
                          ],
                        )),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.LOGIN);
                              },
                              child: Container(
                                  padding: EdgeInsets.only(right: 30),
                                  child: Text(
                                    "SE CONNECTER",
                                    style: kTitleThreeStyle.copyWith(
                                        color: Colors.white),
                                  )),
                            ),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.LOGIN);
                              },
                              child: Container(
                                padding: EdgeInsets.only(left: 30, right: 30),
                                color: kSecondaryColor,
                                height: 60,
                                // width: 200,
                                child: Center(
                                  child: Text(
                                    "S'ENREGISTRER",
                                    style: kTitleThreeStyle,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                widget.isHome
                    ? Container(
                        height: 900,
                        child: Column(
                          children: [
                            navBar(context),
                            SizedBox(
                              height: 400,
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                child: ShowUp(
                                  child: Column(
                                    children: [
                                      Text(
                                        'UNE NOUVELLE PLATEFORME ROCKET LEAGUE',
                                        style: kCapitalStyle,
                                      ),
                                      Text(
                                        "pensé par des joueurs pour d'autres joueurs",
                                        style: kSubtitle,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                                'assets/background/home_background.png'),
                          ),
                        ),
                      )
                    : navBar(context),
                ConstrainedBox(
                    constraints: BoxConstraints(minHeight: Get.height - 300),
                    child: widget.body),
                footer(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget footer() {
    return Container(
      color: kPrimaryColor,
      height: 100,
      child: Row(
        children: [
          Expanded(child: SizedBox()),
          Expanded(flex: 2, child: Text("Legal mentions")),
          Expanded(child: SizedBox()),
          Expanded(flex: 2, child: Text("Tous droit reservé")),
          Expanded(child: SizedBox()),
          Expanded(
              flex: 2,
              child: Text(
                "By IRONRETURN",
                style: kTextStyle,
              )),
        ],
      ),
    );
  }

  Widget navBar(BuildContext context) {
    return Container(
      height: 140,
      color: Colors.black.withOpacity(0.8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: 60,
          ),
          Image.asset(
            'assets/logo/logo_all.png',
            height: 60,
          ),
          Expanded(
              child: Container(
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                // AnimatedPositioned(child: Container(height: 140, width: 80,), duration: Duration(seconds: 1)),
                Container(
                  height: 140,
                ),
                AnimatedPositioned(
                    curve: Curves.decelerate,
                    right: rightPosNavBarSelected,
                    height: 140,
                    width: 210,
                    child: ClipPath(
                        clipper: NavBarClipper(),
                        child: Container(
                          color: kSecondaryColor,
                        )),
                    duration: Duration(milliseconds: 300)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    navBarElement(0, text: "HOME", routeName: Routes.ACCUEIL,
                        onTap: () {
                      setState(() {
                        rightPosNavBarSelected = 738;
                        index = 0;
                      });
                    }),
                    navBarElement(1,
                        text: "SCRIM",
                        routeName: Routes.SCRIM,
                        context: context, onTap: () {
                      setState(() {
                        rightPosNavBarSelected = 538;
                        index = 1;
                      });
                    }),
                    navBarElement(2,
                        text: "TRAINING",
                        routeName: Routes.TRAINING, onTap: () {
                      setState(() {
                        rightPosNavBarSelected = 293;
                        index = 2;
                      });
                    }, context: context),
                    navBarElement(3,
                        text: "CONTACT",
                        routeName: Routes.CONTACT,
                        context: context, onTap: () {
                      setState(() {
                        rightPosNavBarSelected = 30;
                        index = 3;
                      });
                    }),
                  ],
                ),
              ],
            ),
          )),
        ],
      ),
    );
  }

  Widget navBarElement(int indexx,
      {String text, Function() onTap, String routeName, BuildContext context}) {
    return InkWell(
      onTap: () {
        redirect(routeName);
      },
      onHover: (isHover) async {
        if (isHover) {
          if (!changePage) {
            onTap();
          }
        } else {
          if (!changePage) {
            setState(() {
              initNavBar();
              if (indexx == 2) {
                rightPosNavBarSelected = 293;
              }
            });
          }
        }
        if (indexx == 2) {
          XTutorialMenu.show(
            context,
            onHide: () {
              setState(() {
                initNavBar();
              });
            },
            marginTop: marginTop.value,
            marginRight: 293,
            allItem: [
              ShowMenuItem(routes: Routes.TRAININGWORKSHOP, title: "Workshop"),
              ShowMenuItem(
                routes: Routes.TRAININGCODE,
                title: "Training code",
              ),
              ShowMenuItem(
                routes: Routes.TRAININGVIDEO,
                title: "Video tuto",
              ),
            ],
          );
          index = 2;
        }
        print(index.toString() + indexx.toString());
        // redirect(routeName);
      },
      child: Container(
        // color: Colors.red,
        padding: EdgeInsets.only(left: 60, right: 60),
        child: Center(
          child: Text(
            text,
            style: navBarTextStyle.copyWith(
                color: index == indexx ? Colors.black : Colors.white),
          ),
        ),
      ),
    );
  }

  void redirect(String routes) async {
    changePage = true;
    await Future.delayed(Duration(milliseconds: 300));
    Get.toNamed(routes);
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
