import 'package:cherchernom/presentation/views/web/accueil/accueil_view.dart';
import 'package:cherchernom/presentation/views/web/accueil/accueil_view_controller_bindings.dart';
import 'package:cherchernom/presentation/views/web/contact/contact_view.dart';
import 'package:cherchernom/presentation/views/web/contact/contact_view_controller_bindings.dart';
import 'package:cherchernom/presentation/views/web/login/login_view.dart';
import 'package:cherchernom/presentation/views/web/login/login_view_controller_bindings.dart';
import 'package:cherchernom/presentation/views/web/scrim/scrim_view.dart';
import 'package:cherchernom/presentation/views/web/scrim/scrim_view_controller_bindings.dart';
import 'package:cherchernom/presentation/views/web/training%20_workshop/training_workshop_view.dart';
import 'package:cherchernom/presentation/views/web/training%20_workshop/training_workshop_view_controller_bindings.dart';
import 'package:cherchernom/presentation/views/web/training/training_view.dart';
import 'package:cherchernom/presentation/views/web/training/training_view_controller_bindings.dart';
import 'package:cherchernom/presentation/views/web/training_code/training_code_view.dart';
import 'package:cherchernom/presentation/views/web/training_code/training_code_view_controller_bindings.dart';
import 'package:cherchernom/presentation/views/web/training_video/training_video_view.dart';
import 'package:cherchernom/presentation/views/web/training_video/training_video_view_controller_bindings.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'routes.dart';

class Nav {
  static Route<dynamic> routesGenerate(RouteSettings route) {
    List<String> splitRoute = [];
    String nameRoute = route.name;

    splitRoute = route.name.replaceFirst('/', '').split('/');
    nameRoute = splitRoute[0];
    nameRoute = '/' + nameRoute;

    switch (nameRoute) {
      case Routes.ACCUEIL:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.ACCUEIL),
          routeName: Routes.ACCUEIL,
          page: () => AccueilView(),
          binding: AccueilViewControllerBindings(),
        );
        break;
      case Routes.LOGIN:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.LOGIN),
          routeName: Routes.LOGIN,
          page: () => LoginView(),
          binding: LoginViewControllerBindings(),
        );
        break;
      case Routes.TRAINING:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.TRAINING),
          routeName: Routes.TRAINING,
          page: () => TrainingView(),
          binding: TrainingViewControllerBindings(),
        );
        break;
      case Routes.TRAININGCODE:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.TRAININGCODE),
          routeName: Routes.TRAININGCODE,
          page: () => TrainingCodeView(),
          binding: TrainingCodeViewControllerBindings(),
        );
        break;
      case Routes.TRAININGWORKSHOP:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.TRAININGWORKSHOP),
          routeName: Routes.TRAININGWORKSHOP,
          page: () => TrainingWorkshopView(),
          binding: TrainingWorkshopViewControllerBindings(),
        );
        break;
      case Routes.TRAININGVIDEO:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.TRAININGVIDEO),
          routeName: Routes.TRAININGVIDEO,
          page: () => TrainingVideoView(),
          binding: TrainingVideoViewControllerBindings(),
        );
        break;
      case Routes.SCRIM:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.SCRIM),
          routeName: Routes.SCRIM,
          page: () => ScrimView(),
          binding: ScrimViewControllerBindings(),
        );
        break;
      case Routes.CONTACT:
        return GetPageRoute(
          settings: RouteSettings(name: Routes.CONTACT),
          routeName: Routes.CONTACT,
          page: () => ContactView(),
          binding: ContactViewControllerBindings(),
        );
        break;
    }
  }

  static List<GetPage> routes = [
    /// REVIEW [Web] routes

    GetPage(
      name: Routes.ACCUEIL,
      page: () => AccueilView(),
      binding: AccueilViewControllerBindings(),
    ),
    GetPage(
      name: Routes.TRAINING,
      page: () => TrainingView(),
      binding: TrainingViewControllerBindings(),
    ),
    GetPage(
      name: Routes.CONTACT,
      page: () => ContactView(),
      binding: ContactViewControllerBindings(),
    ),
    GetPage(
      name: Routes.LOGIN,
      page: () => LoginView(),
      binding: LoginViewControllerBindings(),
    ),
    GetPage(
      name: Routes.TRAININGWORKSHOP,
      page: () => TrainingWorkshopView(),
      binding: TrainingWorkshopViewControllerBindings(),
    ),
    GetPage(
      name: Routes.TRAININGCODE,
      page: () => TrainingCodeView(),
      binding: TrainingCodeViewControllerBindings(),
    ),
    GetPage(
      name: Routes.TRAININGVIDEO,
      page: () => TrainingVideoView(),
      binding: TrainingVideoViewControllerBindings(),
    ),
    GetPage(
      name: Routes.SCRIM,
      page: () => ScrimView(),
      binding: ScrimViewControllerBindings(),
    ),
  ];
}
