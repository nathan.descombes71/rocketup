class Routes {
  static String get initialRoute {
    return ACCUEIL;
  }

  static const ACCUEIL = '/home';
  // static const TEAMS = '/login';
  // static const NEWS = '/news';
  // static const CONTACT = '/contact';
  static const LOGIN = '/login';
  static const TRAINING = '/training';
  static const REGISTER = '/register';
  static const CONTACT = '/contact';
  static const TEST = '/test';
  static const MANAGER = '/manager';
  static const ARTICLE = '/article';
  static const TRAININGVIDEO = '/training/video';
  static const TRAININGCODE = '/training/code';
  static const TRAININGWORKSHOP = '/training/workshop';
  static const PLAYERRANK = '/playerRank';
  static const SCRIM = '/scrim';
}
