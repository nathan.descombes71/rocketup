// import 'package:cherchernom/domain/feature/user/entities/user.dart'
//     as user;
// import 'package:cherchernom/presentation/core/styles/styles.dart';
// import 'package:cherchernom/presentation/navigation/routes.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:get/get.dart';

// class AuthController {
//   FirebaseFirestore firestore = FirebaseFirestore.instance;

//   Future<String> signIn(String email, String password) async {
//     try {
//       await FirebaseAuth.instance
//           .signInWithEmailAndPassword(email: email, password: password);

//       FirebaseAuth auth = FirebaseAuth.instance;
//       CollectionReference users = firestore.collection('utilisateurs');

//       Get.find<user.User>().pseudo = await users
//           .doc(auth.currentUser.uid)
//           .get()
//           .then((value) => value['Pseudo']);

//       Get.find<user.User>().isManager = await users
//           .doc(auth.currentUser.uid)
//           .get()
//           .then((value) => value['manager']);

//       print(Get.find<user.User>().isManager);

//       if (auth.currentUser != null) {
//         print(auth.currentUser.email);
//       }

//       Get.offAndToNamed(Routes.ACCUEIL);
//       print("ici");

//       succesSnackBar(text: 'Connexion réussi !');

//       return "";
//     } on FirebaseAuthException catch (e) {
//       print(e.code);
//       if (e.code == 'user-not-found') {
//         print('No user found for that email.');
//         return "E-mail ou Mot de passe incorrect";
//       } else if (e.code == 'wrong-password') {
//         return "E-mail ou Mot de passe incorrect";
//       } else if (e.code == 'invalid-email') {
//         return "E-mail ou Mot de passe incorrect";
//       } else if (e.code == 'network-request-failed') {
//         return 'problème de connexion, veuillez réessayer ultèrieurement';
//       } else {
//         return 'problème de connexion, veuillez réessayer ultèrieurement';
//       }
//     } catch (e) {
//       print(e);
//       return 'Problème lors de la connexion';
//     }
//   }

//   // Future<String> register(String email, String password, String pseudo) async {
//   //   try {
//   //     CollectionReference users =
//   //         FirebaseFirestore.instance.collection('utilisateurs');
//   //     UserCredential actualUser = await FirebaseAuth.instance
//   //         .createUserWithEmailAndPassword(email: email, password: password);

//   //     await users.doc(actualUser.user.uid).set(
//   //           ({'Pseudo': pseudo, 'email': email, 'manager': false}),
//   //         );

//   //     succesSnackBar(text: 'Inscription réussi !');

//   //     return "";
//   //   } on FirebaseAuthException catch (e) {
//   //     if (e.code == 'email-already-in-use') {
//   //       return "E-mail déja utilisé";
//   //     } else {
//   //       return '';
//   //     }
//   //   } catch (e) {
//   //     print(e);
//   //     return "Inscriptions échoué";
//   //   }
//   // }

//   signOut() {
//     FirebaseAuth.instance.signOut();
//     succesSnackBar(text: 'Déconnexion réussi !');
//   }
// }
