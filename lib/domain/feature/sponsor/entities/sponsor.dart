// import 'package:abyssesportwebsite/infrastructure/dtos/sponsor_dto/sponsor_dto.dart';

// class Sponsor {
//   int id;
//   String linkRedirect;
//   String pathImage;
//   String nameSponsor;

//   Sponsor({
//     this.id,
//     this.linkRedirect,
//     this.nameSponsor,
//     this.pathImage,
//   });
// }

// extension OnSponsor on Sponsor {
//   Sponsor copyWith({
//     int id,
//     String linkRedirect,
//     String pathImage,
//     String nameSponsor,
//   }) {
//     return Sponsor(
//       id: id ?? this.id,
//       linkRedirect: linkRedirect ?? this.linkRedirect,
//       nameSponsor: nameSponsor ?? this.nameSponsor,
//       pathImage: pathImage ?? this.pathImage,
//     );
//   }

//   SponsorDto get toDto {
//     return SponsorDto(
//       id: id,
//       linkRedirect: linkRedirect,
//       pathImage: pathImage,
//       nameSponsor: nameSponsor,
//     );
//   }
// }

// extension OnListSponsor on List<Sponsor> {
//   List<SponsorDto> get toDto {
//     List<SponsorDto> _sponsorDtoList = [];

//     this?.forEach((entity) => _sponsorDtoList.add(entity.toDto));
//     return _sponsorDtoList;
//   }
// }

// extension OnListSponsorDto on List<SponsorDto> {
//   List<Sponsor> get toEntity {
//     List<Sponsor> _sponsorList = [];

//     this?.forEach((dto) => _sponsorList.add(dto.toEntity));
//     return _sponsorList;
//   }
// }

// extension OnSponsorDto on SponsorDto {
//   Sponsor get toEntity {
//     return Sponsor(
//       id: id,
//       linkRedirect: linkRedirect,
//       nameSponsor: nameSponsor,
//       pathImage: pathImage,
//     );
//   }
// }
