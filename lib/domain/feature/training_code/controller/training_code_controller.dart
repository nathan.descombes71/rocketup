import 'package:cherchernom/domain/feature/training_code/entities/training_code.dart';

class TrainingCodeController {
  List<TrainingCode> listTrainingCode = [
    TrainingCode(
      id: 1,
      value: 'BF61-4151-6D95-8C55',
      name: "Freestyle",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 2,
      value: '5A65-4073-F310-5495',
      name: "Wall to air",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 3,
      value: 'C7E0-9E0B-B739-A899',
      name: "Aerial Shot",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 4,
      value: '2E23-ABD5-20C6-DBD4',
      name: "Saves",
      theme: ThemeTrainingCode.Defense
    ),
      TrainingCode(
      id: 5,
      value: 'CAFC-FB3E-3C0F-B8F1',
      name: "Double tap playground",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 6,
      value: 'ECE7-F71A-5F48-7B46',
      name: "AirDribble",
      theme: ThemeTrainingCode.Attaque,
    ),
       TrainingCode(
      id: 7,
      value: 'FA24-B2B7-2E8E-193B',
      name: "ultimate Warm up",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 8,
      value: 'D87A-9A7A-DBD9-78AF',
      name: "MustyFlick",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 9,
      value: 'E4A0-8342-8007-E328',
      name: "Misa's Training Pack 2",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 10,
      value: '8D93-C997-0ACD-8416',
      name: "Aerial Shot Redirect",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 11,
      value: '2D89-9321-42D2-48BA',
      name: "Bronze Silver Training",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 12,
      value: '42BF-686D-E047-574B',
      name: "Shots You Shouldn't miss",
      theme: ThemeTrainingCode.Attaque,
    ),
  TrainingCode(
      id: 13,
      value: '9E4B-814F-96F1-F978',
      name: "Noob grand champ progression",
      theme: ThemeTrainingCode.Defense
    ),
      TrainingCode(
      id: 14,
      value: '0FB6-C16F-D26D-0A6F',
      name: "AirDribbles (wall ground)",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 15,
      value: '22C6-636F-E52C-D9E7',
      name: "AirDribble mastery",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 16,
      value: '79B1-C81A-521A-B98D',
      name: "Gold Plat",
      theme: ThemeTrainingCode.Defense
    ),
      TrainingCode(
      id: 17,
      value: '5CCE-FB29-7B05-A0B1',
      name: "Why you suck",
      theme: ThemeTrainingCode.Defense,
    ),
      TrainingCode(
      id: 18,
      value: '9F6D-4387-4C57-2E4B',
      name: "Wall shot",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 19,
      value: '5BFE-60D6-0D59-79F2',
      name: "Aerial off wall",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 20,
      value: '7028-5E10-88EF-E83E',
      name: "PowerShots",
      theme: ThemeTrainingCode.Attaque,
    ),
      TrainingCode(
      id: 21,
      value: '3B40-CE8C-58EB-32B3',
      name: "Platinum Diamond mechanic",
      theme: ThemeTrainingCode.Attaque,
    ),
  ];
}