class TrainingCode {
  int id;
  ThemeTrainingCode theme;
  String name;
  String value;

  TrainingCode({
    this.id,
    this.theme,
    this.value,
    this.name,
  });
}

extension OnTrainingCode on TrainingCode {
  TrainingCode copyWith({
    int id,
    ThemeTrainingCode theme,
    String name,
    String value,
  }) {
    return TrainingCode(
      id: id ?? this.id,
      theme: theme ?? this.theme,
      value: value ?? this.value,
      name: name??this.name,
    );
  }

  // PlayerDto get toDto {
  //   return PlayerDto(
  //     id: id,
  //     age: age,
  //     cardPath: cardPath,
  //     country: country,
  //     isStaff: isStaff,
  //     jeu: jeu,
  //     pseudo: pseudo,
  //     role: role,
  //     roleStaff: roleStaff,
  //   );
  // }
}


  enum ThemeTrainingCode {
    Defense,
    Attaque,
    All,
  }


// extension OnListPlayers on List<Players> {
//   List<PlayerDto> get toDto {
//     List<PlayerDto> _playersDtoList = [];

//     this?.forEach((entity) => _playersDtoList.add(entity.toDto));
//     return _playersDtoList;
//   }
// }

// extension OnListPlayersDto on List<PlayerDto> {
//   List<Players> get toEntity {
//     List<Players> _playersList = [];

//     this?.forEach((dto) => _playersList.add(dto.toEntity));
//     return _playersList;
//   }
// }

// extension OnPlayersDto on PlayerDto {
//   Players get toEntity {
//     return Players(
//       id: id,
//       age: age,
//       cardPath: cardPath,
//       country: country,
//       isStaff: isStaff,
//       jeu: jeu,
//       pseudo: pseudo,
//       role: role,
//       roleStaff: roleStaff,
//     );
//   }
// }
