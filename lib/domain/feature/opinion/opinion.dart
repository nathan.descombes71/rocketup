class Opinion {
  int id;
  double note;
  String desc;

  Opinion({
    this.id,
    this.note,
    this.desc,
  });
}

extension OnOpinion on Opinion {
  Opinion copyWith({
    int id,
    String desc,
    double note,
  }) {
    return Opinion(
      id: id ?? this.id,
      desc: desc ?? this.desc,
      note: note ?? this.note,
    );
  }
  //    PlayerDto get toDto {
  //   return PlayerDto(
  //     id: id,
  //     age: age,
  //     cardPath: cardPath,
  //     country: country,
  //     isStaff: isStaff,
  //     jeu: jeu,
  //     pseudo: pseudo,
  //     role: role,
  //     roleStaff: roleStaff,
  //     );
  // }
}

// extension OnListPlayers on List<Players> {
//   List<PlayerDto> get toDto {
//     List<PlayerDto> _playersDtoList = [];

//     this?.forEach((entity) => _playersDtoList.add(entity.toDto));
//     return _playersDtoList;
//   }
// }

// extension OnListPlayersDto on List<PlayerDto> {
//   List<Players> get toEntity {
//     List<Players> _playersList = [];

//     this?.forEach((dto) => _playersList.add(dto.toEntity));
//     return _playersList;
//   }
// }

// extension OnPlayersDto on PlayerDto {
//   Players get toEntity {
//     return Players(
//       id: id,
//       age: age,
//       cardPath: cardPath,
//       country: country,
//       isStaff: isStaff,
//       jeu: jeu,
//       pseudo: pseudo,
//       role: role,
//       roleStaff: roleStaff,
//     );
//   }
// }
