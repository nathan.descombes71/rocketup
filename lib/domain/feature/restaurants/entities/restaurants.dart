import 'package:cherchernom/domain/feature/opinion/opinion.dart';

class Restaurants {
  int id;
  String name;
  List<Opinion> allOpinion;
  int priceLevel;
  double globallyNote;
  

  Restaurants({
    this.id,
    this.name,
    this.allOpinion,
  });
}

extension OnRestaurants on Restaurants {
  Restaurants copyWith({int id, String name, List<Opinion> allOpinion}) {
    return Restaurants(
      id: id ?? this.id,
      name: name ?? this.name,
      allOpinion: allOpinion ?? this.allOpinion,
    );
  }
  //    PlayerDto get toDto {
  //   return PlayerDto(
  //     id: id,
  //     age: age,
  //     cardPath: cardPath,
  //     country: country,
  //     isStaff: isStaff,
  //     jeu: jeu,
  //     pseudo: pseudo,
  //     role: role,
  //     roleStaff: roleStaff,
  //     );
  // }
}

// extension OnListPlayers on List<Players> {
//   List<PlayerDto> get toDto {
//     List<PlayerDto> _playersDtoList = [];

//     this?.forEach((entity) => _playersDtoList.add(entity.toDto));
//     return _playersDtoList;
//   }
// }

// extension OnListPlayersDto on List<PlayerDto> {
//   List<Players> get toEntity {
//     List<Players> _playersList = [];

//     this?.forEach((dto) => _playersList.add(dto.toEntity));
//     return _playersList;
//   }
// }

// extension OnPlayersDto on PlayerDto {
//   Players get toEntity {
//     return Players(
//       id: id,
//       age: age,
//       cardPath: cardPath,
//       country: country,
//       isStaff: isStaff,
//       jeu: jeu,
//       pseudo: pseudo,
//       role: role,
//       roleStaff: roleStaff,
//     );
//   }
// }
