import 'package:cherchernom/domain/feature/training_code/entities/training_code.dart';
import 'package:cherchernom/domain/feature/training_video/entities/training_video.dart';
import 'package:cherchernom/domain/feature/training_worskshop/entities/training_workshop.dart';

class TrainingVideoController {
  List<TrainingVideo> listTrainingVideo = [
    TrainingVideo(
      id: 1,
      author: "Rasmelthor",
      videoID: "QQNNOOXZjiM",
      hexaColor: "BB2513",
      linkPDP: "https://yt3.ggpht.com/ytc/AKedOLQCJJ1DuR4jfAR6cPhqkxAU-ecBdAs4w_Mai6WveQ=s900-c-k-c0x00ffffff-no-rj",
      nameVideo: "Les tirs",
    ),
    TrainingVideo(
      id: 2,
      author: "SniiperRl",
      videoID: "RGlOMvx7JVg",
      hexaColor: "27FFE5",
      linkPDP: "https://yt3.ggpht.com/ytc/AKedOLR00kb6-lz9237i4bO5VxV2bVfL5UlCS3_C_rRL9w=s900-c-k-c0x00ffffff-no-rj",
      nameVideo: "Flip reset"
    ),
    TrainingVideo(
      id: 3,
      author: "LeCheps",
       videoID: "M8bv-Sw8kQQ",
       hexaColor: "15C6B4",
       linkPDP: "https://yt3.ggpht.com/ytc/AKedOLR5Pyu-_szQLbal-wNb3M0T4GsfQLwG7fzOCQGGdw=s900-c-k-c0x00ffffff-no-rj",
       nameVideo: "Musty Flick"

    ),
    TrainingVideo(
      id: 4,
      author: "Azuma",
   videoID: "bGm3A",
   hexaColor: "FFFFFF",
   linkPDP: "https://static-cdn.jtvnw.net/jtv_user_pictures/2158bba4-5857-4281-bd11-4309e8df9814-profile_image-300x300.png",
   nameVideo: "Breezi flick"
    ),
  ];
}
