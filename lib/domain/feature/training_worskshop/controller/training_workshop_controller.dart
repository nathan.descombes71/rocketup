import 'package:cherchernom/domain/feature/training_code/entities/training_code.dart';
import 'package:cherchernom/domain/feature/training_worskshop/entities/training_workshop.dart';

class TrainingWorkshopController {
  List<TrainingWorkshop> listTrainingWorkshop = [
    TrainingWorkshop(
      id: 1,
      imageLink:
          "https://steamuserimages-a.akamaihd.net/ugc/1008149006328256298/AE30C3ACF9C345501B6F1B79F3666C30E519C9DD/?imw=637&imh=358&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=true",
      name: "Speed Jump: Rings 3 - By dmc",
      url: "https://steamcommunity.com/sharedfiles/filedetails/?id=1671658424",
    ),
    TrainingWorkshop(
      id: 2,
      imageLink:
          'https://steamuserimages-a.akamaihd.net/ugc/1695025913828190836/435571438B863066BB61C4CF991AF734FA33276E/?imw=637&imh=358&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=true',
      name: "Lethamyr's Tiny Rings Map",
      url: "https://steamcommunity.com/sharedfiles/filedetails/?id=2541758604",
    ),
    TrainingWorkshop(
      id: 3,
      imageLink:
          'https://steamuserimages-a.akamaihd.net/ugc/1615093798976681783/BB01B762A0964B8BE5682D868A54DB0310661422/?imw=637&imh=358&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=true',
      name: "Intermediate (or not) Dribble Map",
      url:
          "https://steamcommunity.com/sharedfiles/filedetails/?id=2611039001&searchtext=",
    ),
    TrainingWorkshop(
      id: 4,
      imageLink:
          'https://steamuserimages-a.akamaihd.net/ugc/793127482996180511/77AA88ADDC3BAB31D05D507A7F0EC86E282C6B93/?imw=637&imh=358&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=true',
      name: "Aim training - By CoCo",
      url:
          "https://steamcommunity.com/sharedfiles/filedetails/?id=1906378036&searchtext=",
    ),
  ];
}
