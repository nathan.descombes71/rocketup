class TrainingWorkshop {
  int id;
  String imageLink;
  String name;
  String url;

  TrainingWorkshop({
    this.id,
    this.imageLink,
    this.url,
    this.name,
  });
}

extension OnTrainingWorkshop on TrainingWorkshop {
  TrainingWorkshop copyWith({
    int id,
    String name,
    String imageLink,
    String url,
  }) {
    return TrainingWorkshop(
      id: id ?? this.id,
      name: name ?? this.name,
      imageLink: imageLink ?? this.imageLink,
      url: url ?? this.url,
    );
  }

  // PlayerDto get toDto {
  //   return PlayerDto(
  //     id: id,
  //     age: age,
  //     cardPath: cardPath,
  //     country: country,
  //     isStaff: isStaff,
  //     jeu: jeu,
  //     pseudo: pseudo,
  //     role: role,
  //     roleStaff: roleStaff,
  //   );
  // }
}



// extension OnListPlayers on List<Players> {
//   List<PlayerDto> get toDto {
//     List<PlayerDto> _playersDtoList = [];

//     this?.forEach((entity) => _playersDtoList.add(entity.toDto));
//     return _playersDtoList;
//   }
// }

// extension OnListPlayersDto on List<PlayerDto> {
//   List<Players> get toEntity {
//     List<Players> _playersList = [];

//     this?.forEach((dto) => _playersList.add(dto.toEntity));
//     return _playersList;
//   }
// }

// extension OnPlayersDto on PlayerDto {
//   Players get toEntity {
//     return Players(
//       id: id,
//       age: age,
//       cardPath: cardPath,
//       country: country,
//       isStaff: isStaff,
//       jeu: jeu,
//       pseudo: pseudo,
//       role: role,
//       roleStaff: roleStaff,
//     );
//   }
// }
