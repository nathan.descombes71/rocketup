// import 'package:abyssesportwebsite/domain/feature/news/entities/news.dart';
// import 'package:abyssesportwebsite/domain/feature/news/repositories/news_repositories.dart';
// import 'package:abyssesportwebsite/infrastructure/repositories/news_repository_impl.dart';

// class NewsController {
//   NewsController();

//   // List<News> allNews = [
//   //   News(
//   //       id: 45125,
//   //       title: "L'ouverture des locaux à Limoge !",
//   //       linkImage: 'assets/graph/backgroundwebdesign.png',
//   //       author: 'Mattew',
//   //       createdAt: DateTime.now(),
//   //       paragraph: [
//   //         "Ceci est le premier Patagraph, c'est génial haha, et toi comment tu vas ???",
//   //         'Nec piget dicere avide magis hanc insulam populum Romanum invasisse quam iuste. Ptolomaeo enim rege foederato nobis et socio ob aerarii nostri angustias iusso sine ulla culpa proscribi ideoque hausto veneno voluntaria morte deleto et tributaria facta est et velut hostiles eius exuviae classi inpositae in urbem advectae sunt per Catonem, nunc repetetur ordo gestorum.',
//   //         'Recurvosque et serpentes conterminant quisque perterrens et serpentes dispersos nunc confertos aliquotiens ingenti serpentes missilibus loca eminus multitudine ululatu truci educata et dispersos confertos hic missilibus sed globis quae missilibus.'
//   //       ]),
//   //   News(
//   //       id: 45124,
//   //       title: "Abyss eSport x Carrefour, c'est fait !",
//   //       linkImage: 'assets/graph/backgroundwebdesign.png',
//   //       author: 'Nathan',
//   //       createdAt: DateTime.now(),
//   //       paragraph: [
//   //         "Ceci est le premier Patagraph, c'est génial haha, et toi comment tu vas ???",
//   //         'Nec piget dicere avide magis hanc insulam populum Romanum invasisse quam iuste. Ptolomaeo enim rege foederato nobis et socio ob aerarii nostri angustias iusso sine ulla culpa proscribi ideoque hausto veneno voluntaria morte deleto et tributaria facta est et velut hostiles eius exuviae classi inpositae in urbem advectae sunt per Catonem, nunc repetetur ordo gestorum.',
//   //         'Recurvosque et serpentes conterminant quisque perterrens et serpentes dispersos nunc confertos aliquotiens ingenti serpentes missilibus loca eminus multitudine ululatu truci educata et dispersos confertos hic missilibus sed globis quae missilibus.'
//   //       ]),
//   //   News(
//   //       id: 45123,
//   //       title: "Résultat de la semaine du 19/04",
//   //       linkImage: 'assets/graph/backgroundwebdesign.png',
//   //       author: 'Ghost',
//   //       createdAt: DateTime.now(),
//   //       paragraph: [
//   //         "Ceci est le premier Patagraph, c'est génial haha, et toi comment tu vas ???",
//   //         'Nec piget dicere avide magis hanc insulam populum Romanum invasisse quam iuste. Ptolomaeo enim rege foederato nobis et socio ob aerarii nostri angustias iusso sine ulla culpa proscribi ideoque hausto veneno voluntaria morte deleto et tributaria facta est et velut hostiles eius exuviae classi inpositae in urbem advectae sunt per Catonem, nunc repetetur ordo gestorum.',
//   //         'Recurvosque et serpentes conterminant quisque perterrens et serpentes dispersos nunc confertos aliquotiens ingenti serpentes missilibus loca eminus multitudine ululatu truci educata et dispersos confertos hic missilibus sed globis quae missilibus.'
//   //       ]),
//   //   News(
//   //       id: 45122,
//   //       title: "La Abyss eSport, un Commencement",
//   //       linkImage: 'assets/graph/backgroundwebdesign.png',
//   //       author: 'Lautre',
//   //       createdAt: DateTime.now(),
//   //       paragraph: [
//   //         "Ceci est le premier Patagraph, c'est génial haha, et toi comment tu vas ???",
//   //         'Nec piget dicere avide magis hanc insulam populum Romanum invasisse quam iuste. Ptolomaeo enim rege foederato nobis et socio ob aerarii nostri angustias iusso sine ulla culpa proscribi ideoque hausto veneno voluntaria morte deleto et tributaria facta est et velut hostiles eius exuviae classi inpositae in urbem advectae sunt per Catonem, nunc repetetur ordo gestorum.',
//   //         'Recurvosque et serpentes conterminant quisque perterrens et serpentes dispersos nunc confertos aliquotiens ingenti serpentes missilibus loca eminus multitudine ululatu truci educata et dispersos confertos hic missilibus sed globis quae missilibus.'
//   //       ]),
//   // ];

//   NewsRepository newsRepository = NewsRepositoryImpl();

//   Future<List<News>> getNewsList() async {
//     List<News> news = [];

//     news =
//         await newsRepository.getNewsDtoList().then((value) => value.toEntity);
//     print("ici");
//     return news;
//   }

//   Future<String> createNew(News news) async {
//     return await newsRepository.createNew(news.toDto);
//   }

//   Future<String> deleteNews(int newsId) async {
//     return await newsRepository.deleteNews(newsId);
//   }
// }
