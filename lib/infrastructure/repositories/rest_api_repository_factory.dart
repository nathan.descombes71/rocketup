
import 'package:cherchernom/infrastructure/repositories/rest_api_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class RestApiRepositoryFactory<D> extends RestApiRepository {
  RestApiRepositoryFactory({
    @required String controller,
    GetHttpClient client,
  }) : super(
          controller: controller,
          client: client,
        );
}
