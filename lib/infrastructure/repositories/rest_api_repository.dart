import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class RestApiRepository {
  final GetHttpClient client;
  @protected
  final String _controller;

  String get controller => _controller;

  RestApiRepository({
    this.client,
    String controller,
  }) : _controller = controller;
}
