// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'palmares_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
PalmaresDto _$PalmaresDtoFromJson(Map<String, dynamic> json) {
  return _PalmaresDto.fromJson(json);
}

/// @nodoc
class _$PalmaresDtoTearOff {
  const _$PalmaresDtoTearOff();

// ignore: unused_element
  _PalmaresDto call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String nameCup,
      @JsonKey(name: 'position') int position}) {
    return _PalmaresDto(
      id: id,
      nameCup: nameCup,
      position: position,
    );
  }

// ignore: unused_element
  PalmaresDto fromJson(Map<String, Object> json) {
    return PalmaresDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $PalmaresDto = _$PalmaresDtoTearOff();

/// @nodoc
mixin _$PalmaresDto {
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'name')
  String get nameCup;
  @JsonKey(name: 'position')
  int get position;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $PalmaresDtoCopyWith<PalmaresDto> get copyWith;
}

/// @nodoc
abstract class $PalmaresDtoCopyWith<$Res> {
  factory $PalmaresDtoCopyWith(
          PalmaresDto value, $Res Function(PalmaresDto) then) =
      _$PalmaresDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String nameCup,
      @JsonKey(name: 'position') int position});
}

/// @nodoc
class _$PalmaresDtoCopyWithImpl<$Res> implements $PalmaresDtoCopyWith<$Res> {
  _$PalmaresDtoCopyWithImpl(this._value, this._then);

  final PalmaresDto _value;
  // ignore: unused_field
  final $Res Function(PalmaresDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object nameCup = freezed,
    Object position = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      nameCup: nameCup == freezed ? _value.nameCup : nameCup as String,
      position: position == freezed ? _value.position : position as int,
    ));
  }
}

/// @nodoc
abstract class _$PalmaresDtoCopyWith<$Res>
    implements $PalmaresDtoCopyWith<$Res> {
  factory _$PalmaresDtoCopyWith(
          _PalmaresDto value, $Res Function(_PalmaresDto) then) =
      __$PalmaresDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String nameCup,
      @JsonKey(name: 'position') int position});
}

/// @nodoc
class __$PalmaresDtoCopyWithImpl<$Res> extends _$PalmaresDtoCopyWithImpl<$Res>
    implements _$PalmaresDtoCopyWith<$Res> {
  __$PalmaresDtoCopyWithImpl(
      _PalmaresDto _value, $Res Function(_PalmaresDto) _then)
      : super(_value, (v) => _then(v as _PalmaresDto));

  @override
  _PalmaresDto get _value => super._value as _PalmaresDto;

  @override
  $Res call({
    Object id = freezed,
    Object nameCup = freezed,
    Object position = freezed,
  }) {
    return _then(_PalmaresDto(
      id: id == freezed ? _value.id : id as int,
      nameCup: nameCup == freezed ? _value.nameCup : nameCup as String,
      position: position == freezed ? _value.position : position as int,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_PalmaresDto implements _PalmaresDto {
  const _$_PalmaresDto(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'name') this.nameCup,
      @JsonKey(name: 'position') this.position});

  factory _$_PalmaresDto.fromJson(Map<String, dynamic> json) =>
      _$_$_PalmaresDtoFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'name')
  final String nameCup;
  @override
  @JsonKey(name: 'position')
  final int position;

  @override
  String toString() {
    return 'PalmaresDto(id: $id, nameCup: $nameCup, position: $position)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PalmaresDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.nameCup, nameCup) ||
                const DeepCollectionEquality()
                    .equals(other.nameCup, nameCup)) &&
            (identical(other.position, position) ||
                const DeepCollectionEquality()
                    .equals(other.position, position)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(nameCup) ^
      const DeepCollectionEquality().hash(position);

  @JsonKey(ignore: true)
  @override
  _$PalmaresDtoCopyWith<_PalmaresDto> get copyWith =>
      __$PalmaresDtoCopyWithImpl<_PalmaresDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_PalmaresDtoToJson(this);
  }
}

abstract class _PalmaresDto implements PalmaresDto {
  const factory _PalmaresDto(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String nameCup,
      @JsonKey(name: 'position') int position}) = _$_PalmaresDto;

  factory _PalmaresDto.fromJson(Map<String, dynamic> json) =
      _$_PalmaresDto.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'name')
  String get nameCup;
  @override
  @JsonKey(name: 'position')
  int get position;
  @override
  @JsonKey(ignore: true)
  _$PalmaresDtoCopyWith<_PalmaresDto> get copyWith;
}
