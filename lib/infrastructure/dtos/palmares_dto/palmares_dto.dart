import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'palmares_dto.freezed.dart';
part 'palmares_dto.g.dart';

@freezed
abstract class PalmaresDto with _$PalmaresDto {
  const factory PalmaresDto({
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'name') String nameCup,
    @JsonKey(name: 'position') int position,
  }) = _PalmaresDto;
  factory PalmaresDto.fromJson(Map<String, dynamic> json) =>
      _$PalmaresDtoFromJson(json);
}

extension OnPalmaresJson on Map<String, dynamic> {
  PalmaresDto get toPalmaresDto {
    return PalmaresDto.fromJson(this);
  }
}

extension OnListPalmaresJson on List<Map<String, dynamic>> {
  List<PalmaresDto> get toPalmaresDto {
    return map<PalmaresDto>((Map<String, dynamic> e) => e.toPalmaresDto)
        .toList();
  }
}
