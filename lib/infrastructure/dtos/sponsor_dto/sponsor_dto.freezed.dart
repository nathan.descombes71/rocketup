// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'sponsor_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
SponsorDto _$SponsorDtoFromJson(Map<String, dynamic> json) {
  return _SponsorDto.fromJson(json);
}

/// @nodoc
class _$SponsorDtoTearOff {
  const _$SponsorDtoTearOff();

// ignore: unused_element
  _SponsorDto call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name_sponsor') String nameSponsor,
      @JsonKey(name: 'link_redirect') String linkRedirect,
      @JsonKey(name: 'path_image') String pathImage}) {
    return _SponsorDto(
      id: id,
      nameSponsor: nameSponsor,
      linkRedirect: linkRedirect,
      pathImage: pathImage,
    );
  }

// ignore: unused_element
  SponsorDto fromJson(Map<String, Object> json) {
    return SponsorDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $SponsorDto = _$SponsorDtoTearOff();

/// @nodoc
mixin _$SponsorDto {
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'name_sponsor')
  String get nameSponsor;
  @JsonKey(name: 'link_redirect')
  String get linkRedirect;
  @JsonKey(name: 'path_image')
  String get pathImage;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $SponsorDtoCopyWith<SponsorDto> get copyWith;
}

/// @nodoc
abstract class $SponsorDtoCopyWith<$Res> {
  factory $SponsorDtoCopyWith(
          SponsorDto value, $Res Function(SponsorDto) then) =
      _$SponsorDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name_sponsor') String nameSponsor,
      @JsonKey(name: 'link_redirect') String linkRedirect,
      @JsonKey(name: 'path_image') String pathImage});
}

/// @nodoc
class _$SponsorDtoCopyWithImpl<$Res> implements $SponsorDtoCopyWith<$Res> {
  _$SponsorDtoCopyWithImpl(this._value, this._then);

  final SponsorDto _value;
  // ignore: unused_field
  final $Res Function(SponsorDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object nameSponsor = freezed,
    Object linkRedirect = freezed,
    Object pathImage = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      nameSponsor:
          nameSponsor == freezed ? _value.nameSponsor : nameSponsor as String,
      linkRedirect: linkRedirect == freezed
          ? _value.linkRedirect
          : linkRedirect as String,
      pathImage: pathImage == freezed ? _value.pathImage : pathImage as String,
    ));
  }
}

/// @nodoc
abstract class _$SponsorDtoCopyWith<$Res> implements $SponsorDtoCopyWith<$Res> {
  factory _$SponsorDtoCopyWith(
          _SponsorDto value, $Res Function(_SponsorDto) then) =
      __$SponsorDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name_sponsor') String nameSponsor,
      @JsonKey(name: 'link_redirect') String linkRedirect,
      @JsonKey(name: 'path_image') String pathImage});
}

/// @nodoc
class __$SponsorDtoCopyWithImpl<$Res> extends _$SponsorDtoCopyWithImpl<$Res>
    implements _$SponsorDtoCopyWith<$Res> {
  __$SponsorDtoCopyWithImpl(
      _SponsorDto _value, $Res Function(_SponsorDto) _then)
      : super(_value, (v) => _then(v as _SponsorDto));

  @override
  _SponsorDto get _value => super._value as _SponsorDto;

  @override
  $Res call({
    Object id = freezed,
    Object nameSponsor = freezed,
    Object linkRedirect = freezed,
    Object pathImage = freezed,
  }) {
    return _then(_SponsorDto(
      id: id == freezed ? _value.id : id as int,
      nameSponsor:
          nameSponsor == freezed ? _value.nameSponsor : nameSponsor as String,
      linkRedirect: linkRedirect == freezed
          ? _value.linkRedirect
          : linkRedirect as String,
      pathImage: pathImage == freezed ? _value.pathImage : pathImage as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_SponsorDto implements _SponsorDto {
  const _$_SponsorDto(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'name_sponsor') this.nameSponsor,
      @JsonKey(name: 'link_redirect') this.linkRedirect,
      @JsonKey(name: 'path_image') this.pathImage});

  factory _$_SponsorDto.fromJson(Map<String, dynamic> json) =>
      _$_$_SponsorDtoFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'name_sponsor')
  final String nameSponsor;
  @override
  @JsonKey(name: 'link_redirect')
  final String linkRedirect;
  @override
  @JsonKey(name: 'path_image')
  final String pathImage;

  @override
  String toString() {
    return 'SponsorDto(id: $id, nameSponsor: $nameSponsor, linkRedirect: $linkRedirect, pathImage: $pathImage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SponsorDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.nameSponsor, nameSponsor) ||
                const DeepCollectionEquality()
                    .equals(other.nameSponsor, nameSponsor)) &&
            (identical(other.linkRedirect, linkRedirect) ||
                const DeepCollectionEquality()
                    .equals(other.linkRedirect, linkRedirect)) &&
            (identical(other.pathImage, pathImage) ||
                const DeepCollectionEquality()
                    .equals(other.pathImage, pathImage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(nameSponsor) ^
      const DeepCollectionEquality().hash(linkRedirect) ^
      const DeepCollectionEquality().hash(pathImage);

  @JsonKey(ignore: true)
  @override
  _$SponsorDtoCopyWith<_SponsorDto> get copyWith =>
      __$SponsorDtoCopyWithImpl<_SponsorDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_SponsorDtoToJson(this);
  }
}

abstract class _SponsorDto implements SponsorDto {
  const factory _SponsorDto(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name_sponsor') String nameSponsor,
      @JsonKey(name: 'link_redirect') String linkRedirect,
      @JsonKey(name: 'path_image') String pathImage}) = _$_SponsorDto;

  factory _SponsorDto.fromJson(Map<String, dynamic> json) =
      _$_SponsorDto.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'name_sponsor')
  String get nameSponsor;
  @override
  @JsonKey(name: 'link_redirect')
  String get linkRedirect;
  @override
  @JsonKey(name: 'path_image')
  String get pathImage;
  @override
  @JsonKey(ignore: true)
  _$SponsorDtoCopyWith<_SponsorDto> get copyWith;
}
