// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sponsor_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SponsorDto _$_$_SponsorDtoFromJson(Map<String, dynamic> json) {
  return _$_SponsorDto(
    id: json['id'] as int,
    nameSponsor: json['name_sponsor'] as String,
    linkRedirect: json['link_redirect'] as String,
    pathImage: json['path_image'] as String,
  );
}

Map<String, dynamic> _$_$_SponsorDtoToJson(_$_SponsorDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name_sponsor': instance.nameSponsor,
      'link_redirect': instance.linkRedirect,
      'path_image': instance.pathImage,
    };
