// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_NewsDto _$_$_NewsDtoFromJson(Map<String, dynamic> json) {
  return _$_NewsDto(
    id: json['id'] as int,
    author: json['author'] as String,
    linkImage: json['link_image'] as String,
    paragraph: (json['paragraph'] as List)?.map((e) => e as String)?.toList(),
    createdAt: json['created_at'] as String,
    title: json['title'] as String,
  );
}

Map<String, dynamic> _$_$_NewsDtoToJson(_$_NewsDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'author': instance.author,
      'link_image': instance.linkImage,
      'paragraph': instance.paragraph,
      'created_at': instance.createdAt,
      'title': instance.title,
    };
